-----------------------------------------------------------------------------------------
--
-- levelsmenu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals

-- Load png library for extracting info from a png without opening it
local pngLib = require("pngLib")

local backBtn = nil

local levelone = nil

local levels = {}

local deleteAudio = function ()
	print("Deleting Audio")
	audio.stop()
	--audio.dispose( menuMusic )
	audio.dispose( selectButton )
	--menuMusic = nil
	--print(menuMusic)
	selectButton = nil
end

local loadLevel = function ()
	deleteAudio()
	storyboard.gotoScene("reset", "fade", 500 )
end

-- 'onRelease' event listener for playBtn
local function onBackBtnRelease()
	
	-- go to level1.lua scene
	_G.currentLevel = "planetsmenu"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

local onButtonRelease = function( event )
	local btn = event.target

        _G.currentLevelIndex = btn.level
	if(btn.level == 1) and (_G.currentWorld == "jungle") then
		local GGData = require( "modules.GGData" );
		
		local story = GGData:new( "story" );
		
		local storyPlayed = story:get("storyPlayed");
		
		if(storyPlayed == true) then
			_G.currentLevel = "levelloader";
		else
			story:set("storyPlayed", true);
			story:save();
			_G.currentLevel = "storypage1";
		end
	else
		_G.currentLevel = "levelloader"
	end
	audio.play(selectButton, {onComplete=loadLevel})
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	print("loading menu")
	--menuMusic = audio.loadStream("sfx/menumusic.wav")
	selectButton = audio.loadStream("sfx/select.wav")
	
	--audio.play( menuMusic, {loops=-1} )
	
	-- display a background image
	local background = display.newImageRect( "interface/" .. _G.currentWorld .. "levels.jpg", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	--local testData = {
	--	["level1"] = {
	--		unlocked = true,
	--		highscore = 0
	--		},
	--	["level2"] = {
	--		unlocked = false,
	--		highscore = 0
	--		}
	--}
	
	
	local GGData = require( "modules.GGData" )
	
	local level = GGData:new( _G.currentWorld .. "Data" )
	
	--level.level1 = 0
	--level.level2 = 0
	--level.level3 = 0

	--level:save()
	
	
	for i=1,10 do
		local gameLevel = level:get("level" .. i)
		
		if(gameLevel ~= nil) then
	
			levels[i] = widget.newButton{
				defaultFile="interface/levels/" .. i .. ".png",
				overFile="interface/levels/" .. i .. "Over.png",
				width=pngLib.getPngInfo("interface/levels/" .. i .. ".png").width,
				height=pngLib.getPngInfo("interface/levels/" .. i .. ".png").height,				
				onRelease = onButtonRelease
			}
			
			-- No padlock
			levels[i].padlock = nil
				
		else
			levels[i] = display.newImageRect("interface/levels/" .. i .. ".png",pngLib.getPngInfo("interface/levels/" .. i .. ".png").width,pngLib.getPngInfo("interface/levels/" .. i .. ".png").height)
			
			-- Insert the padlocks
			levels[i].padlock = display.newImageRect("interface/levels/padlock.png",pngLib.getPngInfo("interface/levels/padlock.png").width,pngLib.getPngInfo("interface/levels/padlock.png").height)
		end
		
		
		levels[i]:setReferencePoint( display.TopLeftReferencePoint )		
			
		if(i == 1) then
			levels[i].x = 74
			levels[i].y = 124		
		elseif(i == 6) then
			levels[i].x = levels[1].x
			levels[i].y = levels[1].y + levels[i].height		
		else
			levels[i].x = levels[i-1].x + levels[i].width
			levels[i].y = levels[i-1].y		
		end
		
		-- Position the padlocks
		if(i < 6) and (levels[i].padlock ~= nil) then
			levels[i].padlock.x = levels[i].x + (levels[i].width / 1.8)
			levels[i].padlock.y = levels[i].y + (levels[i].height-levels[i].padlock.height)
		elseif (i > 5) and (levels[i].padlock ~= nil) then
			levels[i].padlock.x = levels[i].x + (levels[i].width / 2)
			levels[i].padlock.y = levels[i].y + (levels[i].height-(levels[i].padlock.height/2))
		end		
		
		levels[i].level = i
		
	end
	
	-- Back button
	-- create a widget button (which will load planetsmenu.lua on release)
	backBtn = widget.newButton{
		defaultFile="interface/back.png",
		overFile="interface/backOver.png",
		width=pngLib.getPngInfo("interface/back.png").width,
		height=pngLib.getPngInfo("interface/back.png").height,	
		onRelease = onBackBtnRelease	-- event listener function
	}
	backBtn:setReferencePoint( display.TopLeftReferencePoint )
	backBtn.x = 20
	backBtn.y = display.contentHeight - (backBtn.height + 20)	
	
	-- all display objects must be inserted into group
	group:insert( background )
	
	for i=1,#levels do
		group:insert( levels[i] )
		if(levels[i].padlock ~= nil) then
			group:insert( levels[i].padlock )
		end
	end
	
	group:insert( backBtn)
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	

	
	if storyBtn then
		storyBtn:removeSelf()	-- widgets must be manually removed
		storyBtn = nil
	end
	
	if jungle then
		jungle:removeSelf()
		jungle = nil
	end
	
	if exitBtn then
		exitBtn:removeSelf()
		exitBtn = nil
	end
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )


-----------------------------------------------------------------------------------------

return scene