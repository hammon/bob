-----------------------------------------------------------------------------------------
--
-- howtoplay.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals

-- Load png library for extracting info from a png without opening it
local pngLib = require("pngLib")

local nextBtn = nil

local menuMusic = nil

local deleteAudio = function ()
	print("Deleting Audio")
	audio.stop()
	audio.dispose( menuMusic )
	audio.dispose( selectButton )
	menuMusic = nil
	print(menuMusic)
	selectButton = nil
end

local loadLevel = function ()
	deleteAudio()
	storyboard.gotoScene("reset", "fade", 250 )
end

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	_G.currentLevel = "planetsmenu"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	print("loading how to play")
	menuMusic = audio.loadStream("sfx/menumusic.wav")
	selectButton = audio.loadStream("sfx/select.wav")
	
	--audio.play( menuMusic, {loops=-1} )
	
	-- display a background image
	local background = display.newImageRect( "interface/mainmenu/howtoplay.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0

	
	-- create a widget button (which will load planetsmenu.lua on release)
	nextBtn = widget.newButton{
		defaultFile="interface/next.png",
		overFile="interface/nextOver.png",
		width=pngLib.getPngInfo("interface/next.png").width,
		height=pngLib.getPngInfo("interface/next.png").height,	
		onRelease = onPlayBtnRelease	-- event listener function
	}
	nextBtn:setReferencePoint( display.TopLeftReferencePoint )
	nextBtn.x = 427
	nextBtn.y = 5
	
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( nextBtn )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if nextBtn then
		nextBtn:removeSelf()
		nextBtn = nil
	end
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )


-----------------------------------------------------------------------------------------

return scene