-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "physics" library
local physics = require "physics"
--physics.setDrawMode("hybrid")
physics.start(); physics.pause()

-- Require the widget library
local widget = require( "widget" )

--------------------------------------------

-- forward declarations and other locals

-- Screen settings
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5

-- json data files

-- physics data files

-- Level not yet complete
local levelComplete = false

-- Display Groups
local gameGroup = nil
local hudGroup = nil
local bkgGroup = nil
local levelOverGroup = nil

-- hit points
local WALLHITPOINTS = 4
local PLANTHITPOINTS = 8
local PLATFORMHITPOINTS = 3



-- Game entities
local mrtoggles, theDoor, theDoorClosed
local bob = nil

-- Baddies --
local baddies = {}


-- HUD
local buttonRight, buttonLeft, blastButton
-- Health bar
local healthBar = nil
local healthBarRect = nil

-- Countdown Timer Variables
local timerCnt = 60
local timerText = "1:00"
local timeOver = false
local textTimer = nil
local timesUp

-- Level Over Buttons
local menuButton, restartButton


-- Audio
local thrustSound
local ambience
local boom
local tickerSound
local toggles

-- Ticker flag -- set to 1 once 10 seconds left on the clock 
local ticker = 0

-- activate multitouch so multiple touches can press different buttons simultaneously
system.activate( "multitouch" )

------------------------------------------------------
-- Game Loop Functions
------------------------------------------------------

-- Move Baddies Function
local moveBaddies = function ()
    -- Loop through all the baddies in the table and move them left and right
    for i=1,#baddies do
        if baddies[i].x > baddies[i].endX or baddies[i].x < baddies[i].startX then
            baddies[i].moveSpeed = baddies[i].moveSpeed * -1
            baddies[i].xScale = baddies[i].xScale * -1
            if baddies[i].flipped then
                baddies[i].flipped = false
                physics.removeBody( baddies[i])
                physics.addBody( baddies[i], "static", planetPhysics:get(baddies[i].myName))
            else
                baddies[i].flipped = true
                physics.removeBody( baddies[i])
                physics.addBody( baddies[i], "static", planetPhysics:get(baddies[i].myName .. "flipped"))                
            end
        end

        baddies[i].x = baddies[i].x + baddies[i].moveSpeed
    end	
		
end

-- Countdown Timer Function
local countDown = function()

    if not timeOver then
            timerCnt = timerCnt - 1
            --if bob.timer > 0 then
                --bob.timer = bob.timer - 1
            --else
            --    bob.text.text = ""
            --    bob.text.alpha = 0
            --end
    end
    
    if timerCnt > 9 then
            timerText = "0:" .. timerCnt
    elseif timerCnt < 0 then
            timeOver = true
    else
            ticker = ticker + 1
            timerText = "0:0" .. timerCnt
    end
    
    if ticker == 1 then
        audio.play( tickerSound, {channel=4, loops=0} )
    end
end

local deleteAudio = function ()
        audio.stop()
        audio.dispose( ambience )
        audio.dispose( tickerSound )
        audio.dispose( boom )
        audio.dispose ( thrust)
        audio.dispose ( toggles )
        ambience = nil
        tickerSound = nil
        boom = nil
        thrust = nil
        toggles = nil
        print("Audio Deleted")
end

local restartPressed = false

local endLevel = function ()
        timesUpGroup.alpha = 0.8
        hudGroup.alpha = 0.0
        
        if levelComplete then
            bob.alpha = 0
            timesUp.text = "Completed!"
        end
                       
        if menuButton.pressed then
            -- Restart the level
            if restartPressed == false then
                restartPressed = true
                print("*** Menu Pressed ***")
                deleteAudio()
                _G.currentLevel = "mainmenu"
                storyboard.gotoScene( "reset", "fade", 250 )
             end
        elseif restartButton.pressed then
            -- Restart the level
            if restartPressed == false then
                restartPressed = true
                deleteAudio()
                print("*** Resetting Level ***")
                _G.currentLevel = "scrolltest"
                storyboard.gotoScene( "reset", "fade", 250 )
             end
        end
end

local restarted = false

local startOver = function()
        print("**** Restarting....")
        hudGroup.alpha = 0.0
        deleteAudio()
        _G.currentLevel = "scrolltest"
        storyboard.gotoScene( "reset", "fade", 250 )    
end

------------------------------------------------------
-- Runtime enterFrame Event handler
------------------------------------------------------

-- Main Game Loop
local mainLoop = function ()
        -- If time is up or bob is dead
         if timeOver or bob.dead then
            
            if(restarted == false) then
                startOver()
                restarted = true
            end
            return true
        elseif levelComplete then -- If the level is complete, end the level
            endLevel()
            return true
        end

	--We want to follow the bob if he starts to get off the screen.
	if bob.y<-gameGroup.y+3/5*display.contentHeight/2 and gameGroup.y < 0 then 
		gameGroup.y=3/5*display.contentHeight/2-bob.y
                --bkgGroup.y = 2/5*display.contentHeight/2
	end
	if bob.y>-gameGroup.y+3/5*display.contentHeight and gameGroup.y > -display.contentHeight then 
		gameGroup.y=3/5*display.contentHeight-bob.y
                --bkgGroup.y = 2/5*display.contentHeight
	end
	if bob.x<-gameGroup.x+2/5*display.contentWidth and gameGroup.x < 0 then 
		gameGroup.x=2/5*display.contentWidth-bob.x
                --bkgGroup.x = gameGroup.x
	end
	if bob.x>-gameGroup.x+2/5*display.contentWidth and gameGroup.x > -display.contentWidth then 
		gameGroup.x=2/5*display.contentWidth-bob.x
                --bkgGroup.x = gameGroup.x
	end
        
        -- Update countdown timer text
        txtTimer.text = timerText
        
        -- Move the baddies
        moveBaddies()        
        
        local xForce = 0
        local yForce = 0
        
        -- Increase upward force for thrust
        if blastButton.pressed then
            audio.play( thrustSound, {channel=1} )
    
            yForce = -20     
        else

            audio.stop({ channel=1 } )
        end
        
        -- Increase force to the right if the right button is pressed
        if buttonRight.pressed then
            bob.xScale = 1 
            xForce = bob.xForce
        end
        
        -- Increase force to the left if the left button is pressed        
        if buttonLeft.pressed then
            bob.xScale = -1 
            xForce = -bob.xForce
        end           
      
        bob.linearDamping = 1
        if bob.deadBody >= 1 then
            if bob.deadBody == 1 then
                physics.removeBody(bob)
                bob.deadBody = 2
            end
        else
            bob:applyForce(xForce,yForce, bob.x, bob.y)
        end


end

------------------------------------------------------
--  Button handler
------------------------------------------------------

local buttonHandler = function( self, event )
    if event.phase == "began" then      
   	
        print("began phase")        
        
        self.pressed = true
        
        -- Subsequent touch events will target button even if they are outside the stageBounds of button
        display.getCurrentStage():setFocus( self, event.id )
        self.isFocus = true 
        event.target.alpha = event.target.alphaActive
		               
    elseif self.isFocus then
        local bounds = self.stageBounds
        local x,y = event.x,event.y
        local isWithinBounds = 
            bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
        
        if isWithinBounds and event.phase == "moved" then
            self.pressed = true
        elseif not isWithinBounds or event.phase == "ended" or event.phase == "cancelled" then
            self.isFocus = false
            display.getCurrentStage():setFocus( nil )
            self.pressed = false
            event.target.alpha = event.target.alphaNotActive
        end       
    end	
	
    return true
end

------------------------------------------------------
-- Collision handler
------------------------------------------------------

-- Set Bob to dead
local bobDead = function ()
    bob.dead = true
end

-- Kill Bob animation
local killBob = function ()
    audio.stop({channel=1})
    bob.deadBody = 1
    transition.to(bob,{alpha=0, time=500})
    audio.play( boom, {channel=3, onComplete=bobDead} )
end

-- Reduce Bob's Health
local reduceHealth = function( amount )
    --bob.health = bob.health - amount
    healthBarRect.width = healthBarRect.width + amount

    healthBarRect:setReferencePoint( display.TopRightReferencePoint )
    healthBarRect.x = healthBar.x + healthBar.width
    
    if ( healthBarRect.width >= healthBar.width ) then
        killBob()
    end
end

-- Open the door
local openDoor = function ()
    if theDoor.open == false then
        theDoor.open = true
        transition.to(mrtoggles, {alpha=0, time=500})
        transition.to(theDoorClosed, {alpha=0, time=1000})
    end
end

-- Complete the level
local completeLevel = function ()
    if theDoor.open then
        levelComplete = true
    end
end

-- Stick to platform and initiate Bob's internal countdown timer
local stickyPlatform = function ()
    bob.timer = 10
    print("Sticky")
end

-- On Collision Handler
local onLocalCollision = function( self, event )
        if ( event.phase == "began" ) then
 
                print( "Collision began")
                
               -- print(bob.x)
                --self.action( self.hitAmount )
                local fixtureId = planetPhysics:getFixtureId("level1", event.selfElement)
                print(fixtureId)
                if (fixtureId == "branchhurt") or (fixtureId == "leafhurt") then
                    reduceHealth( 4 )
                end
                if(fixtureId == "planthurt") then
                    reduceHealth( 8 )
                end
                if(self.myName == "flyer" or self.myName == "thefloor") then
                    --audio.stop({channel=1})
                   -- bob.deadBody = 1
                   -- transition.to(bob,{alpha=0, time=500})
                   -- audio.play( boom, {channel=3, onComplete=self.action} )
                   self.action()

                end
                if(self.myName == "mrtoggles") then
                    if(theDoor.open == false) then
                        transition.to(mrtoggles, {alpha=0, time=500})
                        audio.play(toggles, {channel=5, onComplete=self.action})
                    end
                end
                if(self.myName == "thedoor") then
                    if(theDoor.open) then                        
                        transition.to(bob, {alpha=0, time=1000, onComplete=self.action})
                    end
                end
 
        elseif ( event.phase == "ended" ) then
 
                print("Collision ended" )
 
        end
        
end

-- jsonFile() loads json file & returns contents as a string
local jsonFile = function( filename, base )
	
	-- set default base dir if none specified
	if not base then base = system.ResourceDirectory; end
	
	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )
	
	-- will hold contents of file
	local contents
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	if file then
	   -- read all contents of file into a string
	   contents = file:read( "*a" )
	   io.close( file )	-- close the file after using it
	end
	
	return contents
end


-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
        
        -- use json
        json = require("json" )
        
        print("Loading Game Data...")
        
        -- load the game data  (general across the game)
        --local gameData = json.decode( jsonFile ( "levels/gamedata.json" ) )
        
        -- load in planet data (general planet-wide baddies, etc)
        --local worldData = json.decode ( jsonFile ( "levels/jungle/worlddata.json" )
                                       
        -- load in specific level data
        local levelData = json.decode( jsonFile( "levels/jungle/level1.json" ) )
        
        print("Loading Game Physics...")
        
        -- open the game physics (general across the game)
        gamePhysics = (require "gamephysics").gamePhysics(1)
        
        -- open the current planet physics data (baddies, etc)
        worldPhysics = (require "levels.jungle.planetphysics").worldPhysics(1)
        
        -- open ithe current level physics data
        levelPhysics = (require "levels.jungle.level1").levelPhysics(1)
        
        -- Load Sounds
        print("Loading Game Sounds")
        
        -- Game-wide sounds
        thrustSound = audio.loadStream("sfx/boost.wav")
        boom = audio.loadStream("sfx/boom.wav")
        tickerSound = audio.loadStream("sfx/ticker.wav")
        toggles = audio.loadStream("sfx/toggles.wav")
        
        -- World-wide sounds
        ambience = audio.loadStream("sfx/ambience.mp3")
        
    	-- Create display groups
        
        print("Creating Display Groups")
        
        -- Background Group
        bkgGroup = display.newGroup()
        
        -- Game Group 
        gameGroup = display.newGroup()
                
        -- The HUD (buttons, timer, health bar, etc)
        hudGroup = display.newGroup()
        
        -- Display once level is completed / failed
        levelOverGroup = display.newGroup()
        
        print("Playing World Ambience")        
        -- Load level ambience sound
        audio.play( ambience, {channel=2, loops=-1} )
        
        print ("Loading background")
        -- Load current level background image
        local bg = display.newImageRect( "levels/jungle/bg.png", display.contentWidth, display.contentHeight-20 )
	bg:setReferencePoint( display.TopLeftReferencePoint )
	bg.x, bg.y = 0, 0
        
        print("Loading platforms")
	-- load the platforms
	local platforms = display.newImageRect( "levels/jungle/level1.png", display.contentWidth*2, display.contentHeight*2 )
	platforms:setReferencePoint( display.TopLeftReferencePoint )
	platforms.x, platforms.y = 0, 0
        
        
        -- Set current platform/level name
        platforms.myName = worldData.levelName
        
        -- Load in the level current level platform physics
        levelPhyics.addBody( platforms, "static", levelPhysics:get("level1"))
        
        -- Add platform collision
        platforms.collision = onLocalCollision
        platforms:addEventListener( "collision", platforms )
        
        -- Set up level borders
        print("Adding boundaries")
        -- Set border edge physics
        borderBody = { density=1, friction=2.0, bounce=0.0 }
        
        -- Top border
        local borderTop = display.newRect( 0, 0, display.contentWidth*2, 1 )
	borderTop:setFillColor( 0)      
	physics.addBody( borderTop, "static", borderBody )
        
        -- Bottom Border
        local borderBottom = display.newRect( 0, display.contentHeight*2-70, display.contentWidth*2, 1 )
	borderBottom.alpha = 0
        borderBottom.myName = "thefloor"
        
        -- Bottom Border Physics (death on impact)
        borderBottom.collision = onLocalCollision
        borderBottom.action = killBob
        borderBottom:addEventListener( "collision", borderBottom )   
        physics.addBody( borderBottom, "static", borderBody )
        
        -- Left Border
	local borderLeft = display.newRect( 0, 0, 1, display.contentHeight*2 )
	borderLeft:setFillColor(0)
	physics.addBody( borderLeft, "static", borderBody )
	 
        -- Right Border 
	local borderRight = display.newRect( display.contentWidth*2, 0, 1, display.contentHeight*2 )
	borderRight:setFillColor(0)
	physics.addBody( borderRight, "static", borderBody )        
        
        print("Adding Bob")
        -- Create Bob        
	bob = display.newImage("characters/bob.png")
	bob:setReferencePoint(display.CentreLeftReferencePoint)
	bob.x, bob.y = gameData.bob.x, gameData.bob.y
        bob.myName = gameData.bob.name
        bob.dead = gameData.bob.dead
        bob.deadBody = gameData.bob.deadBody
        bob.xForce = gameData.bob.xForce
        
        -- Add Bob's physics
        gamePhysics.addBody( bob, "dynamic", gamePhysics:get(bob.myName))
	bob.isFixedRotation = levelData.bob.fixedRotation
        
        print("Adding Mr Toggles")
        -- Add Mr Toggles
	mrtoggles = display.newImage( "characters/toggles.png" )
        mrtoggles:setReferencePoint(display.CentreLeftReferencePoint)
	mrtoggles.x, mrtoggles.y = 845, 284
        mrtoggles.myName = "mrtoggles"
        
        -- Mr Toggles physics and collision
        physics.addBody( mrtoggles, "static", { density=0, friction=0, bounce=0, isSensor=true } )     
        mrtoggles.collision = onLocalCollision
        mrtoggles.action = openDoor
        mrtoggles:addEventListener( "collision", mrtoggles )
        
        print("Adding portal")
        -- The Door
        theDoor = display.newImage( "characters/dooropen.png")
	theDoor.x, theDoor.y = 42, 154
        theDoor.open = false
        
        -- Door physics and collision
        physics.addBody( theDoor, "static", { density=0, friction=0.6, bounce=0, isSensor=true } )      
        theDoor.collision = onLocalCollision
        theDoor.myName = "thedoor"
        theDoor.action = completeLevel
        theDoor:addEventListener( "collision", theDoor )       
	
        -- Load in closed door image
        theDoorClosed = display.newImage( "characters/doorclosed.png" )
	theDoorClosed.x, theDoorClosed.y = theDoor.x, theDoor.y
        
        print("Populating with Baddies")
        -- Load the baddies
        for i=1,#levelData.baddies do
            baddies[i] = display.newImage("characters/" .. levelData.baddies[i].name .. ".png")
            baddies[i]:setReferencePoint(display.CentreLeftReferencePoint)
            baddies[i].x = levelData.baddies[i].x; baddies[i].y = levelData.baddies[i].y
            baddies[i].myName = levelData.baddies[i].name
            baddies[i].startX = baddies[i].x
            baddies[i].endX = levelData.baddies[i].endX
            baddies[i].moveSpeed = levelData.baddies[i].speed
            baddies[i].flipped = levelData.baddies[i].flipped
            
            -- Add the physics
            physics.addBody( baddies[i], "static", planetPhysics:get(baddies[i].myName))
            baddies[i].collision = onLocalCollision
            baddies[i].action = killBob
            baddies[i]:addEventListener( "collision", baddies[i] )  
        end      
               
        -- Create the HUD       
        print ("Adding Buttons")       
        -- Left / Right Button
        blastButton = display.newImage( "interface/blast.png" )
        blastButton:setReferencePoint( display.BottomRightReferencePoint )
        blastButton.x = display.contentWidth
        blastButton.y = display.contentHeight
        blastButton.alpha = 1
        blastButton.touch = buttonHandler

        -- Thruster Button Event Listener
        blastButton:addEventListener("touch",blastButton)
        blastButton.pressed = false
        blastButton.alphaNotActive = 1
        blastButton.alphaActive = 1       
        
        -- Left Button
        buttonLeft = display.newImage( "interface/leftbutton.png" )
        buttonLeft:setReferencePoint( display.BottomLeftReferencePoint )        
        buttonLeft.x = 0; buttonLeft.y = display.contentHeight
        buttonLeft.touch = buttonHandler
        buttonLeft.alpha = 1
        
        -- Left Button Event Listener
        buttonLeft:addEventListener("touch",buttonLeft)       
        buttonLeft.pressed = false  
        buttonLeft.alphaNotActive = 1
        buttonLeft.alphaActive = 1
        
        -- Right Button
        buttonRight = display.newImage( "interface/rightbutton.png" )
        buttonRight:setReferencePoint( display.BottomLeftReferencePoint )        
        buttonRight.x = buttonLeft.width; buttonRight.y = display.contentHeight
        buttonRight.touch = buttonHandler
        buttonRight.alpha = 1
        
        -- Left Button Event Listener
        buttonRight:addEventListener("touch",buttonRight)       
        buttonRight.pressed = false  
        buttonRight.alphaNotActive = 1
        buttonRight.alphaActive = 1
        
        print("Creating Health Bar")
        -- Health bar image
        healthBar = display.newImage( "interface/healthBar.png" )
        healthBar:setReferencePoint( display.TopLeftReferencePoint )
        healthBar.x = display.contentWidth/2 - healthBar.width/2
        healthBar.y = display.contentHeight-30
        
        -- Health Bar overlay
	healthBarRect = display.newRect(0,0,0,healthBar.height)
        healthBarRect:setReferencePoint( display.TopRightReferencePoint )
	healthBarRect.x = healthBar.x + healthBar.width
        healthBarRect.y = healthBar.y 
        healthBarRect:setFillColor( 0, 0, 0)
                
        -- Setting up countdown timer
        print("Setting Timer")

        -- Load in timer background
        local timerBack = display.newImage( "timer.png" )
        timerBack.x = 48; timerBack.y = 18
        
        -- Create timer text
        txtTimer = display.newText(timerText, 20, 7, "Laserian", 17)
        txtTimer:setTextColor(255,0,0)
        
        -- Tick once a second
        timer.performWithDelay(1000, countDown, 0)        
        
        -- Add Evil Clive image
        local evilClive = display.newImage( "evilclive.png" )
        evilClive.x = timerBack.x + timerBack.width - 28; evilClive.y = 18
        
        -- Level Over display
        print("Adding \"Level Over\" Display")
        
        -- Make the group invisible
        levelOverGroup.alpha = 0.0
        
	-- Setup dark background
        local levelOverBack = display.newRect(0,0,display.contentWidth,display.contentHeight)
	levelOverBack:setReferencePoint( display.TopLeftReferencePoint )
        levelOverBack:setFillColor(black)
	levelOverBack.x, levelOverBack.y = 0, 0                  
        
        levelOverMessage = display.newText("Time's Up!", 0, 0, "Laserian", 30)
        levelOverMessage.x = display.contentWidth * 0.5; timesUp.y = display.contentHeight * 0.5 - 20
        levelOverMessage:setTextColor(255,255,255)        
        
        -- Restart Button
        restartButton = display.newText("Replay", 0, 0, "Laserian", 18)
        restartButton.x = timesUp.x - (timesUp.width / 5); restartButton.y = timesUp.y + 28
        restartButton:setTextColor( 255, 255, 0 )      
        
        restartButton.touch = buttonHandler   
        restartButton:addEventListener("touch",restartButton)       
        restartButton.pressed = false
        restartButton.alphaNotActive = 1.0
        restartButton.alphaActive = 0.5
        
        -- Menu Button
        menuButton = display.newText("Menu", 0, 0, "Laserian", 18)
        menuButton.x = restartButton.x + restartButton.width - 15; menuButton.y = restartButton.y
        menuButton:setTextColor( 255, 255, 0 )      
        
        menuButton.touch = buttonHandler   
        menuButton:addEventListener("touch", menuButton)       
        menuButton.pressed = false
        menuButton.alphaNotActive = 1.0
        menuButton.alphaActive = 0.5           
        
        print("Inserting background")
        -- Insert background image
        bkgGroup:insert(bg)
        
        print("Inserting game entities")        
        gameGroup:insert( platforms )
        gameGroup:insert( borderBottom )
        gameGroup:insert( mrtoggles )
        gameGroup:insert( theDoor )
        gameGroup:insert( theDoorClosed )
        gameGroup:insert( bob )
        for i=1,#baddies do
            gameGroup:insert( baddies[i] )
        end
        
        print("Inserting \"Level Over\" box")
        levelOverGroup:insert( levelOverBack )
        levelOverGroup:insert( levelOverMessage )
        levelOverGroup:insert( restartButton )
        levelOverGroup:insert( menuButton )
        
        print("Inserting HUD")
        hudGroup:insert( blastButton )        
        hudGroup:insert( buttonRight )
        hudGroup:insert( buttonLeft )
        hudGroup:insert( healthBar )
        hudGroup:insert( healthBarRect )        
        hudGroup:insert( timerBack )
        hudGroup:insert( txtTimer )
        hudGroup:insert( evilClive )
        hudGroup.alpha = 0.8

        -- Insert into main group
        group:insert( bkgGroup )
        group:insert( gameGroup )
        group:insert( timesUpGroup)         
        group:insert( hudGroup )
        
        hudGroup:toFront()
        levelOverGroup:toFront() 
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	physics.start()
	physics.setGravity(-10)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
        baddies = nil
        
	physics.stop()
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	package.loaded[physics] = nil
	physics = nil
        
end


-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )



Runtime:addEventListener("enterFrame", mainLoop)

-----------------------------------------------------------------------------------------

return scene