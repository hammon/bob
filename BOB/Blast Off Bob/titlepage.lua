-----------------------------------------------------------------------------------------
--
-- titlepage.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals

local cnt = 1
local backgrounds = {}

local titleSwapTimer = nil


-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------
local titleSwap = function ()

	if cnt < 3 then			
		-- fade out previous image, fade in current image
		transition.to( backgrounds[cnt-1], { time=2000, alpha=0 } )	
		transition.to( backgrounds[cnt], { time=2000, alpha=1 } )	
		
		cnt = cnt + 1
		return true
	else
		timer.cancel(titleSwapTimer)
		-- go to loadingmainmenu.lua scene 		
		storyboard.gotoScene( "mainmenu", "crossFade", 2000 )
		return true
	end

end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	for i=1,2 do
		local imageName = "interface/title/titlepage" .. i .. ".png"
		backgrounds[i] = display.newImageRect( imageName, display.contentWidth, display.contentHeight )
		backgrounds[i]:setReferencePoint( display.TopLeftReferencePoint )
		backgrounds[i].x, backgrounds[i].y = 0, -1			
		backgrounds[i].alpha = 0
		group:insert( backgrounds[i] )
	end

	-- display a background image
	backgrounds[1].alpha = 1
	
	-- Tick once a second
        titleSwapTimer = timer.performWithDelay(3000, titleSwap, 0) 
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )


-----------------------------------------------------------------------------------------

return scene