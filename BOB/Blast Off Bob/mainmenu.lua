-----------------------------------------------------------------------------------------
--
-- mainmenu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals

-- Load png library for extracting info from a png without opening it
local pngLib = require("pngLib")

local playBtn = nil

local fb = nil
local twitter = nil
local sandlancer = nil
local muteButton = nil

local menuMusic = nil

local deleteAudio = function ()
	print("Deleting Audio")
	audio.stop()
	audio.dispose( menuMusic )
	audio.dispose( selectButton )
	menuMusic = nil
	print(menuMusic)
	selectButton = nil
end

local loadLevel = function ()
	deleteAudio()
	storyboard.gotoScene("reset", "fade", 250 )
end

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	_G.currentLevel = "howtoplay"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for storyBtn
local function onStoryBtnRelease()
	
	-- Display the story
	_G.currentLevel = "storypage1"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	print("loading menu")
	menuMusic = audio.loadStream("sfx/menumusic.wav")
	selectButton = audio.loadStream("sfx/select.wav")
	
	audio.play( menuMusic, {loops=-1} )
	
	-- display a background image
	local background = display.newImageRect( "interface/mainmenu/mainmenu.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	-- create a widget button (which will loads level1.lua on release)
	fb = widget.newButton{
		defaultFile="interface/mainmenu/fbButton.png",
		overFile="interface/mainmenu/fbButtonOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/fbButton.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/fbButton.png").height		
		--onRelease = onStoryBtnRelease	-- event listener function
	}
	fb:setReferencePoint( display.TopLeftReferencePoint )
	fb.x = 7
	fb.y = 278
	
	-- create a widget button (which will loads level1.lua on release)
	twitter = widget.newButton{
		defaultFile="interface/mainmenu/twitterButton.png",
		overFile="interface/mainmenu/twitterButtonOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/twitterButton.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/twitterButton.png").height		
		--onRelease = onStoryBtnRelease	-- event listener function
	}
	twitter:setReferencePoint( display.TopLeftReferencePoint )
	twitter.x = 45
	twitter.y = 278
	
	-- create a widget button (which will loads level1.lua on release)
	sandlancer = widget.newButton{
		defaultFile="interface/mainmenu/sandlancerButton.png",
		overFile="interface/mainmenu/sandlancerButtonOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/sandlancerButton.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/sandlancerButton.png").height,			
		--onRelease = onStoryBtnRelease	-- event listener function
	}
	sandlancer:setReferencePoint( display.TopLeftReferencePoint )
	sandlancer.x = 84
	sandlancer.y = 278
	
	-- create a widget button (which will loads level1.lua on release)
	muteButton = widget.newButton{
		defaultFile="interface/mainmenu/muteButton.png",
		overFile="interface/mainmenu/muteButtonOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/muteButton.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/muteButton.png").height,			
		--onRelease = onStoryBtnRelease	-- event listener function
	}
	muteButton:setReferencePoint( display.TopLeftReferencePoint )
	muteButton.x = 446
	muteButton.y = 287	
	
	-- create a widget button (which will load planetsmenu.lua on release)
	playBtn = widget.newButton{
		defaultFile="interface/mainmenu/play.png",
		overFile="interface/mainmenu/playOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/play.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/play.png").height,	
		onRelease = onPlayBtnRelease	-- event listener function
	}
	playBtn:setReferencePoint( display.TopLeftReferencePoint )
	playBtn.x = 155
	playBtn.y = 252
	
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( playBtn )
	group:insert( fb )
	group:insert( twitter )
	group:insert( sandlancer )
	group:insert( muteButton )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()
		playBtn = nil
	end
	
	if fb then
		fb:removeSelf()
		fb = nil
	end
	
	if twitter then
		twitter:removeSelf()
		twitter = nil
	end
	
	if sandlancer then
		sandlancer:removeSelf()
		sandlancer = nil
	end
	
	if muteButton then
		muteButton:removeSelf()
		muteButton = nil
	end
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )


-----------------------------------------------------------------------------------------

return scene