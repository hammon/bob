-----------------------------------------------------------------------------------------
--
-- storypage1.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals
local buttonRight
local cnt = 2
local backgrounds = {}
local backgroundView = nil;

-- 'onRelease' event listener for buttonRight


-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------
local slideCnt = 100;
local slideComplete = false;

local loadLevel = function()
    storyboard.gotoScene( "reset", "fade", 1000 );
end

local nextSlide = function()
    print(slideCnt);
    slideCnt = slideCnt -1;
    if (slideCnt < 0 ) and (slideComplete == false) then		
	if cnt < 6 then			
		-- fade out previous image, fade in current image
		--transition.to( backgrounds[cnt-1], { time=2000, alpha=0 } )	
		transition.to( backgrounds[cnt], { time=1000, x=0 } )	
		
		cnt = cnt + 1
		--return true	-- indicates successful touch
		slideCnt = 100;
	else
		-- go to loadingmainmenu.lua scene 	
        _G.currentLevelIndex = 1;
        _G.currentLevel = "levelloader";
        _G.currentWorld = "jungle";

        transition.to(backgroundView, {time = 500, alpha = 0});
        
        timer.performWithDelay(1000,loadLevel,1);   

		--storyboard.gotoScene( "loadingmainmenu", "fromRight", 1000 )
		
		slideComplete = true;
		--return true	-- indicates successful touch
	end

    end

end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	backgroundView = display.newGroup();
	
	for i=1,5 do
		local imageName = "story/page" .. i .. ".jpg"
		backgrounds[i] = display.newImageRect( imageName, display.contentWidth, display.contentHeight )
		backgrounds[i]:setReferencePoint( display.TopLeftReferencePoint )
		backgrounds[i].x, backgrounds[i].y = display.contentWidth+1, 0			
		--backgrounds[i].alpha = 0
		backgroundView:insert( backgrounds[i] )
	end

	-- display a background image
	backgrounds[1].x = 0

	group:insert(backgroundView);
	
         -- Add a touch event
        --buttonRight = widget.newButton
        --{
        --    id = "right",
        --    defaultFile = "buttonRight.png",
        --    overFile = "buttonRightOver.png",
        --    onRelease = rightRelease
        --}
        
        --buttonRight.x = display.contentWidth - buttonRight.width + 18; buttonRight.y = 293
	
	-- all display objects must be inserted into group
	
	--group:insert( titleLogo )
	--group:insert( buttonRight )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	--if buttonRight then
	--	buttonRight:removeSelf()	-- widgets must be manually removed
	--	buttonRight = nil
	--end
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

Runtime:addEventListener("enterFrame", nextSlide)


-----------------------------------------------------------------------------------------

return scene