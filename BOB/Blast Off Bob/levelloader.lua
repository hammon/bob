-----------------------------------------------------------------------------------------
--
-- levelloader.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "physics" library
local physics = require "physics"
--physics.setDrawMode("hybrid")
physics.start(); physics.pause()

-- Require the widget library
local widget = require( "widget" )

local nextWorld = nil

--------------------------------------------
local paused = false
local pauseScreen = nil
local plant = nil
-- Pause screen buttons
local resumeGameBtn = nil
local muteButton = nil

-- forward declarations and other locals
-- Load png library for extracting info from a png without opening it
local pngLib = require("pngLib")

-- CBE Resources Partcicle Engine
local CBE=require("CBEffects.Library")

-- Particle Emitter for Explosion
local explodeEm = nil
local waterEm = {}
local bobFlame = nil

-- Screen settings
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5

-- json data files
local gameData = nil
local worldData = nil
local levelData = nil

-- physics data files
local levelPhysics = nil
local gamePhysics = nil
local worldPhysics = nil

-- Platforms
local platforms = nil

-- Level not yet complete
local levelComplete = false

local completeScreen = nil;

-- Display Groups
local gameGroup = nil
local hudGroup = nil
local bkgGroup = nil
local levelOverGroup = nil
local pauseGroup = nil
local foregroundGroup = nil
local completeGroup = nil;

-- Toggles Counter
local ToggleCnt = 1;



-- Game entities
local theDoor, theDoorClosed
local bob = nil
local clive = nil
local toggles = {}

-- Baddies --
local baddies = {}
local waterfalls = {}
local imageSheets = {}


-- HUD
local buttonRight, buttonLeft, blastButton
-- Health bar
--local healthBarRect = nil

-- Countdown Timer Variables
local timerCnt = 60
local timerText = "1:00"
local timeOver = false
local textTimer = nil
local levelOverMessage = nil
local countdownTimer = nil

-- Level Over Buttons
local menuButton, restartButton, pauseButton

-- Audio
local thrustSound
local ambience
local boom
local tickerSound
local togglesNoise
local selectButton

-- Ticker flag -- set to 1 once 10 seconds left on the clock 
local ticker = 0

-- activate multitouch so multiple touches can press different buttons simultaneously
system.activate( "multitouch" )

------------------------------------------------------
-- Game Loop Functions
------------------------------------------------------

-- Move Baddies Function
local moveBaddies = function ()
    -- Loop through all the baddies in the table and move them left and right
    for i=1,#baddies do
        if(baddies[i].movementType == "dynamic") then
            if baddies[i].x > baddies[i].endX or baddies[i].x < baddies[i].startX then
                baddies[i].speedX = baddies[i].speedX * -1
                baddies[i].xScale = baddies[i].xScale * -1
                if baddies[i].flipped then
                    baddies[i].flipped = false
                    physics.removeBody( baddies[i])
                    physics.addBody( baddies[i], "static", worldPhysics:get(baddies[i].myName))
                else
                    baddies[i].flipped = true
                    physics.removeBody( baddies[i])
                    physics.addBody( baddies[i], "static", worldPhysics:get(baddies[i].myName .. "flipped"))                
                end
            end
            
            if baddies[i].y > baddies[i].endY or baddies[i].y < baddies[i].startY then
                baddies[i].speedY = baddies[i].speedY * -1
            end        
    
           if baddies[i].speedX < 0 or baddies[i].speedX > 0 then
                baddies[i].x = baddies[i].x + baddies[i].speedX
           end
    
           if baddies[i].speedY < 0 or baddies[i].speedY > 0 then
                baddies[i].y = baddies[i].y + baddies[i].speedY
           end
       end
       
        if(baddies[i].animated) then
            --baddies[i]:play();
        end
    end	
		
end

-- Countdown Timer Function
local countDown = function()
if not paused then
    
    if not timeOver then
            timerCnt = timerCnt - 1
            if bob.timer > 0 then
                bob.timer = bob.timer - 1
                if bob.timer < 2 then
                    bob.text.text = ""
                    bob.text.alpha = 0
                end
            end
    end
    --print( timerCnt )
    if timerCnt > 9 then
            timerText = "0:" .. timerCnt
    elseif timerCnt < 0 then
            timeOver = true
    else
            ticker = ticker + 1
            timerText = "0:0" .. timerCnt
    end
    
    if ticker == 1 then
        audio.play( tickerSound, {channel=4, loops=0} )
    end
end
end

local deleteAudio = function ()
        audio.stop()
        audio.dispose( ambience )
        audio.dispose( tickerSound )
        audio.dispose( boom )
        audio.dispose ( thrust)
        audio.dispose ( togglesNoise )
        audio.dispose ( selectButton )
        ambience = nil
        tickerSound = nil
        boom = nil
        thrust = nil
        togglesNoise = nil
        selectButton = nil
        print("Audio Deleted")
end

local restartPressed = false
local nextLevelSet = false
local levelEnded = false

local endLevel = function ()

        if(levelEnded == false) then

            if(_G.currentLevelIndex == 10) then
                transition.to(completeScreen, {alpha = 0, time = 500});
                levelOverGroup.alpha = 1
            else
                transition.to(levelOverGroup, {alpha = 1, time = 500});
            end

            levelEnded = true;
        end
        --levelOverGroup.alpha = 1
        hudGroup.alpha = 0.0
        
        
        if levelComplete then
            --levelOverMessage.text = "Completed!"
            
            if(nextLevelSet == false) then
            
                local GGData = require( "modules.GGData" )

                if(_G.currentLevelIndex == 10) then
                    _G.currentWorld = nextWorld;

                    local nextLevel = GGData:new( _G.currentWorld .. "Data" )
            
                    nextLevel:set("level" .. 1,2)

                    nextLevel:save()

                    print("World set to: " .. _G.currentWorld)
                    print("Level set to 1");
                else
            
                    local nextLevel = GGData:new( _G.currentWorld .. "Data" )
                
                    nextLevel:set("level" .. _G.currentLevelIndex+1,2)
                    
                    nextLevel:save()

                    print("World is: " .. _G.currentWorld)
                    print("Level set to " .. _G.currentLevelIndex+1)
                end
                

                
                nextLevelSet = true
            end
            

        end
        
        timer.cancel(countdownTimer)
                       
--        if menuButton.pressed then
            -- Restart the level
--            if restartPressed == false then
--                restartPressed = true
--                print("*** Menu Pressed ***")
--                deleteAudio()
--                _G.currentLevel = "mainmenu"
--                storyboard.gotoScene( "reset", "fade", 250 )
--             end
--        elseif restartButton.pressed then
--            -- Restart the level
--            if restartPressed == false then
--                restartPressed = true
--                deleteAudio()
--                print("*** Resetting Level ***")
--                _G.currentLevel = "levelloader"
--                storyboard.gotoScene( "reset", "fade", 250 )
--             end
--        end
end

local loadLevel = function()
        print("Loading next level!")
        storyboard.gotoScene( "reset", "fade", 500 )
end
-- 'onRelease' event listener for playBtn
local function onNextBtnRelease()
        if restartPressed == false then
            restartPressed = true	
            deleteAudio()        
            -- go to next level
            --_G.currentLevelIndex = _G.currentLevelIndex + 1        
            _G.currentLevel = "levelsmenu"
            storyboard.gotoScene( "reset", "fade", 500 )
            --audio.play(selectButton, {onComplete=loadLevel})
        end
	
	return true	-- indicates successful touch
end

local restarted = false

local startOver = function()
        print("**** Restarting....")
        timer.cancel(countdownTimer)
        hudGroup.alpha = 0.0
        deleteAudio()
        _G.currentLevel = "levelloader"
        storyboard.gotoScene( "reset", "fade", 250 )    
end

local loadLevel = function ()
	deleteAudio()
	storyboard.gotoScene("reset", "fade", 250 )
end

-- 'onRelease' event listener for playBtn
local function exitGame()
	
	-- go to level1.lua scene
        print("Test")
	_G.currentLevel = "levelsmenu"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

local resumeGame = function()
    transition.to(pauseGroup, {alpha=0, time=500})
    paused = false
    physics.start()
    audio.resume()
end


------------------------------------------------------
-- Runtime enterFrame Event handler
------------------------------------------------------
--local jumpCnt = 120
local cliveCnt = 0
local waterCnt = false
local waterFlow = false
local completeScreenDisplayed = false

-- Main Game Loop
local mainLoop = function ()
    --plant:play("eat")

    if(pauseButton.pressed and not paused) then
        paused = true
        transition.to(pauseGroup, {alpha=1, time=500})
        physics.pause()
        audio.pause()
    end
    
    if not paused then
 
        -- If time is up or bob is dead
         if timeOver or bob.dead then

            if(restarted == false) then
                startOver()
                restarted = true
            end
            return true
        elseif levelComplete then -- If the level is complete, end the level
            if(_G.currentLevelIndex == 10) then
                if(completeScreenDisplayed == false) then
                    transition.to(completeScreen, {x = 0, time = 500});
                    timer.performWithDelay( 2000, endLevel, 1 );
                    completeScreenDisplayed = true;
                end

            else
                timer.performWithDelay( 500, endLevel, 1);
            end

            return true
        end

	--We want to follow the bob if he starts to get off the screen.
	if bob.y<-gameGroup.y+3/5*display.contentHeight/2 and gameGroup.y < 0 then 
		gameGroup.y=3/5*display.contentHeight/2-bob.y
                --bkgGroup.y = 2/5*display.contentHeight/2
	end
	if bob.y>-gameGroup.y+3/5*display.contentHeight and gameGroup.y > (-platforms.height)+display.contentHeight+10 then
                print("Before: " .. gameGroup.y)
		gameGroup.y=3/5*display.contentHeight-bob.y
                print("After: " .. gameGroup.y)
                --bkgGroup.y = 2/5*display.contentHeight
	end
	if bob.x<-gameGroup.x+2/5*display.contentWidth and gameGroup.x < 0 then 
		gameGroup.x=2/5*display.contentWidth-bob.x
                --bkgGroup.x = gameGroup.x
	end
	if bob.x>-gameGroup.x+2/5*display.contentWidth and gameGroup.x > -platforms.width + (display.contentWidth) then 
		gameGroup.x=2/5*display.contentWidth-bob.x
                --bkgGroup.x = gameGroup.x
	end
        
        -- Update countdown timer text
        txtTimer.text = timerText
        
        -- Move the baddies
        moveBaddies()        
        
        local xForce = 0
        local yForce = 0
        
        -- Increase upward force for thrust
        if blastButton.pressed then
            audio.play( thrustSound, {channel=1} )
            --print(bob.y)
            yForce = bob.yForce

            bobFlame:translate("flame", bob.x, bob.y)
            bobFlame:emitMaster()
            

        else

            audio.stop({ channel=1 } )
        end
        
        -- Increase force to the right if the right button is pressed
        if buttonRight.pressed and bob.timer < 1 then
            if bob.xScale == -1 then
                physics.removeBody( bob )
                physics.addBody( bob, "dynamic", gamePhysics:get(bob.myName))
                bob.isFixedRotation = gameData.bob.fixedRotation
            end
            bob.xScale = 1 
            xForce = bob.xForce
         
        end
        
        -- Increase force to the left if the left button is pressed        
        if buttonLeft.pressed and bob.timer < 1  then
            if bob.xScale == 1 then
                physics.removeBody( bob )
                physics.addBody( bob, "dynamic", gamePhysics:get(bob.myName .. "flipped"))
                bob.isFixedRotation = gameData.bob.fixedRotation
            end
            bob.xScale = -1 
            xForce = -bob.xForce            
        end
        
        if (clive ~= nil ) and (clive.active == true) then
            cliveCnt = cliveCnt -1
            if cliveCnt < 0 then
                clive:applyLinearImpulse(0, -15, clive.x, clive.y)
                cliveCnt = 90
                clive:play();
            end
        end

        if waterFlow == true then
            bob.linearDamping = 10
        else
            bob.linearDamping = 1
        end
        
        if bob.deadBody >= 1 then
            if bob.deadBody == 1 then
                physics.removeBody(bob)
                bob.deadBody = 2
            end
        else
            if bob.timer <= 0 then
                bob.gravityScale = 1
                bob:applyForce(xForce,yForce, bob.x, bob.y)
            elseif bob.timer > 0 then
                bob.gravityScale = 0
                bob.text.alpha = 1
                bob.text.x = bob.x + 2
                bob.text.y = bob.y - (bob.height * 0.5) - 8
                bob.text.text = bob.timer        
            end
        end
    end
    


end

------------------------------------------------------
--  Button handler
------------------------------------------------------

local buttonHandler = function( self, event )

    if event.phase == "began" then      

        print("began phase")        
        self.pressed = true
        
        -- Subsequent touch events will target button even if they are outside the stageBounds of button
        display.getCurrentStage():setFocus( self, event.id )
        self.isFocus = true 
        event.target.alpha = event.target.alphaActive
		               
    elseif self.isFocus then
        local bounds = self.stageBounds
        local x,y = event.x,event.y
        local isWithinBounds = 
            bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
        
        if isWithinBounds and event.phase == "moved" then
            self.pressed = true
        elseif not isWithinBounds or event.phase == "ended" or event.phase == "cancelled" then
            self.isFocus = false
            display.getCurrentStage():setFocus( nil )
            self.pressed = false
            event.target.alpha = event.target.alphaNotActive
        end       
    end	
	
    return true
end

------------------------------------------------------
-- Collision handler
------------------------------------------------------

-- Set Bob to dead
local bobDead = function ()
    bob.dead = true
end

-- Kill Bob animation
local killBob = function ()
    audio.stop({channel=1})
    bob.deadBody = 1
    --transition.to(bob,{alpha=0, time=500})
    bob.alpha = 0
    -- Explosion Animation
    explodeEm:translate ("fire", bob.x, bob.y)
    explodeEm:translate ("smoke", bob.x, bob.y)
    explodeEm:emitMaster();

    audio.play( boom, {channel=3, onComplete=bobDead} )
end

-- Reduce Bob's Health
local reduceHealth = function( amount )

    --bob.health = bob.health - amount
    healthBarRect.width = healthBarRect.width + amount

    healthBarRect:setReferencePoint( display.TopRightReferencePoint )
    healthBarRect.x = healthBar.x + healthBar.width
    
    if ( healthBarRect.width >= healthBar.width ) then
        killBob()
    end
end

-- Open the door
local openDoor = function ()
    if theDoor.open == false and ToggleCnt == #toggles then
        theDoor.open = true
        --transition.to(mrtoggles, {alpha=0, time=500})
        transition.to(theDoorClosed, {alpha=0, time=1000})
    end
end

-- Complete the level
local completeLevel = function ()
    if theDoor.open then
        levelComplete = true
    end
end

-- Stick to platform and initiate Bob's internal countdown timer
local stickyPlatform = function ()
    bob.timer = 5
    print("Sticky")
end

-- On Collision Handler
local onLocalCollision = function( self, event )

        if ( event.phase == "began" ) then
 
                print( "Collision began")
                
                --print(self.myName)
                
               -- print(bob.x)
                --self.action( self.hitAmount )
                print("I collided with " .. event.other.myName)
                --print("I am " .. self.myName)

                
                if event.other.myName == "platforms" then
                    local fixtureId = levelPhysics:getFixtureId("level" .. _G.currentLevelIndex, event.otherElement)
                    --print(fixtureId)
                    if(fixtureId ~= nil) then
                        if (fixtureId == "hurt") and (self.myName == "bob") then
                            killBob();
                        end

                        if(fixtureId=="sticky") then
                            stickyPlatform();
                        end
                    end
                end
                
                if event.other.myName == "flytrap" then
                    local fixtureId = worldPhysics:getFixtureId(event.other.myName, event.otherElement);
                    
                    event.other:play();
                end

                if(event.other.type == 1 or event.other.myName == "thefloor") then
                  event.other.action();

                end
                
                if(event.other.myName == "clive") then
                    local fixtureId = gamePhysics:getFixtureId(event.other.myName, event.otherElement);
                    
                    if (fixtureId == "activate") then
                        clive.active = true;
                    end
                    
                    if(fixtureId == "clive") then
                        event.other.action();
                    end
                end
                    

                if(event.other.myName == "waterfall") then
                    waterFlow = true
                end
                
                if(event.other.myName == "mrtoggles") then
                    if(theDoor.open == false) then
                        transition.to(event.other, {alpha=0, time=500})
                        ToggleCnt = ToggleCnt + 1;
                        print(ToggleCnt);
                        print(#toggles);
                        event.other.myName = "collected"
                        audio.play(togglesNoise, {channel=5, onComplete=event.other.action})
                        
                    end
                end
                
                if(event.other.myName == "thedoor" ) then
                    if(theDoor.open) then
                        physics.removeBody( bob );
                        if(_G.currentLevelIndex == 10) then                        
                            transition.to(theDoor, {alpha=0, time=500, onComplete=event.other.action})
                        else
                            transition.to(bob, {alpha=0, time=1000, onComplete=event.other.action})
                        end
                    end
                end
 
        elseif ( event.phase == "ended" ) then
 
                print("Collision ended" )
                if(event.other.myName == "waterfall") then
                    waterFlow = false
                end                
 
        end
        
end

-- jsonFile() loads json file & returns contents as a string
local jsonFile = function( filename, base )
	
	-- set default base dir if none specified
	if not base then base = system.ResourceDirectory; end
	
	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )
	
	-- will hold contents of file
	local contents
	
	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	if file then
	   -- read all contents of file into a string
	   contents = file:read( "*a" )
	   io.close( file )	-- close the file after using it
	end
	
	return contents
end

local grabImageSheet = function(image, imageSheetData)
            local frames = {};
            
            for i=1,#imageSheetData do
                    -- Grab each frame
                    local frame = {x=imageSheetData[i].frame.x,
                                    y=imageSheetData[i].frame.y,
                                    width=imageSheetData[i].frame.w,
                                    height=imageSheetData[i].frame.h};
                    
                    -- Insert into frames table
                    table.insert(frames, frame);
                    
            end
            
            -- Add the frames table to options
            local options = 
                                {frames = frames};
                                 
                                -- optional params; used for dynamic resolution support
                                options.sheetContentWidth = pngLib.getPngInfo(image).width;
                                options.sheetContentHeight = pngLib.getPngInfo(image).height;
            
            -- Create the imagesheet
            local imageSheet = graphics.newImageSheet( image, options );
            
            return imageSheet;
end


-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
         
        print("Loading Current Level: \n" .. _G.currentWorld .. " planet, Level " .. _G.currentLevelIndex)
        
        timeOver = false
        
        -- use json
        json = require("json" )
        
        print("Loading Game Data...")
        
        -- load the game data  (general across the game)
        gameData = json.decode( jsonFile( "gamedata.json" ) )
        
        -- load in planet data (general planet-wide baddies, etc)
        worldData = json.decode( jsonFile( "levels/" .. _G.currentWorld .. "/worlddata.json" ) )
                                       
        -- load in specific level data
        levelData = json.decode( jsonFile( "levels/" .. _G.currentWorld .. "/level" .. _G.currentLevelIndex .. ".json" ) )
        
        print(levelData.startingPoint.x)
        
        print("Loading Game Physics...")

        -- Generate physics file names        
        local gamePhysicsName = "levels.gamephysics"
        local worldPhysicsName = "levels." .. _G.currentWorld .. ".planetphysics"
        local levelPhysicsName = "levels." .. _G.currentWorld .. ".level" .. _G.currentLevelIndex

        -- Setting next world (if it exists)
        if(levelData.nextWorld ~= nil) then
            nextWorld = levelData.nextWorld
            print("The next world is: " .. nextWorld);
        end

        
        -- open the game physics (general across the game)
        gamePhysics = require (gamePhysicsName).physicsData(0.5)
        
        -- open the current planet physics data (baddies, etc)
        worldPhysics = require (worldPhysicsName).physicsData(0.5)

        -- open the current level physics data (platforms, etc)
        levelPhysics = require (levelPhysicsName).physicsData(0.5)

        -- Load Sounds
        print("Loading Game Sounds")
        
        -- Game-wide sounds
        thrustSound = audio.loadStream("sfx/boost.wav")
        boom = audio.loadStream("sfx/boom.wav")
        tickerSound = audio.loadStream("sfx/ticker.wav")
        togglesNoise = audio.loadStream("sfx/toggles.wav")
        
        -- World-wide sounds
        ambience = audio.loadStream("sfx/" .. _G.currentWorld .. "ambience.mp3") 
        selectButton = audio.loadStream("sfx/select.wav")
        
    	-- Create display groups
        
        print("Creating Display Groups")
        
        -- Background Group
        bkgGroup = display.newGroup()
        
        -- Game Group 
        gameGroup = display.newGroup()
        
        -- ForegroundGroup (for items that appear in the foreground, e.g. waterfalls)
        foregroundGroup = display.newGroup()
                
        -- The HUD (buttons, timer, health bar, etc)
        hudGroup = display.newGroup()
        
        -- The pause screen
        pauseGroup = display.newGroup()
        
        -- Display once level is completed / failed
        completeGroup = display.newGroup();
        levelOverGroup = display.newGroup()

        
        print("Playing World Ambience")        
        -- Load level ambience sound
        audio.play( ambience, {channel=2, loops=-1} )
        
        print ("Loading background")
        -- Load current level background image
        local bg = display.newImageRect( "levels/" .. _G.currentWorld .. "/bg.png", display.contentWidth, display.contentHeight )
    	bg:setReferencePoint( display.TopLeftReferencePoint )
    	bg.x, bg.y = 0, 0
        
        print("Loading platforms")
	   -- load the platforms
        local currentLevelImage = "levels/" .. _G.currentWorld .. "/level" .. _G.currentLevelIndex .. ".png";

	   --local platforms = display.newImageRect( "levels/" .. _G.currentWorld .. "/level" .. _G.currentLevelIndex .. ".png", display.contentWidth*1.2, display.contentHeight*1.2)
	   platforms = display.newImageRect( currentLevelImage, pngLib.getPngInfo(currentLevelImage).width, pngLib.getPngInfo(currentLevelImage).height);
        --pngLib.getPngInfo("
        platforms:setReferencePoint( display.TopLeftReferencePoint )
	   platforms.x, platforms.y = 0, 0
        print("Width: " .. platforms.width .. " Height: " .. platforms.height)
        platforms.myName = "platforms"
        
        -- Set current platform/level name
       -- platforms.myName = worldData.levelName
        
        -- Load in the level current level platform physics
        physics.addBody( platforms, "static", levelPhysics:get("level" .. _G.currentLevelIndex))
        
        -- Add platform collision
        --platforms.collision = onLocalCollision
        --platforms:addEventListener( "collision", platforms )
        
        -- Set up level borders
        print("Adding boundaries")
        -- Set border edge physics
        borderBody = { density=1, friction=2.0, bounce=0.0 }
        
        -- Top border
        local borderTop = display.newRect( 0, 0, platforms.width, 1 )
	borderTop:setFillColor( 0)      
	physics.addBody( borderTop, "static", borderBody )
        borderTop.myName = "top"
        
        -- Bottom Border -30
        local borderBottom = display.newRect( 0, platforms.height-30, platforms.width, 1 )
	borderBottom.alpha = 0
        borderBottom.myName = "thefloor"
        
        -- Bottom Border Physics (death on impact)
        borderBottom.collision = onLocalCollision
        borderBottom.action = killBob
        --borderBottom:addEventListener( "collision", borderBottom )   
        physics.addBody( borderBottom, "static", borderBody )
        
        -- Left Border
	local borderLeft = display.newRect( 0, 0, 1, platforms.width )
	borderLeft:setFillColor(0)
	physics.addBody( borderLeft, "static", borderBody )
        borderLeft.myName = "left"
	 
        -- Right Border 
	local borderRight = display.newRect( platforms.width, 0, 1, platforms.height )
	borderRight:setFillColor(0)
	physics.addBody( borderRight, "static", borderBody )
        borderRight.myName = "right"
        
        print("Adding Bob")
        -- Create Bob        
	bob = display.newImageRect("characters/bob.png", pngLib.getPngInfo("characters/bob.png").width, pngLib.getPngInfo("characters/bob.png").height)
	bob:setReferencePoint(display.CentreLeftReferencePoint)
	bob.x, bob.y = levelData.startingPoint.x, levelData.startingPoint.y
        bob.myName = gameData.bob.name
        bob.timer = 0
        bob.dead = gameData.bob.dead
        bob.deadBody = gameData.bob.deadBody
        bob.xForce = gameData.bob.xForce
        bob.yForce = gameData.bob.yForce
        if(levelData.startingPoint.flipped) then
            bob.xScale = -1
            physics.addBody( bob, "dynamic", gamePhysics:get(bob.myName .. "flipped"))
        else
            physics.addBody( bob, "dynamic", gamePhysics:get(bob.myName))
        end
        
        bob.collision = onLocalCollision
        bob:addEventListener( "collision", bob )
                       
        -- Add Bob's physics
        -- Are we on the Simulator?
        --if ( "simulator" == system.getInfo("environment") ) then
        --    bob.gravityScale = 0.25
        --end
        
	bob.isFixedRotation = gameData.bob.fixedRotation
        
        -- For countdown timer above bob's head
        bob.text = display.newText("", bob.x + (bob.width * 0.5), bob.y - 2, "Laserian", 11)
        bob.text:setTextColor(255,0,0)
        bob.text.alpha = 0

        
        print("Adding Mr Toggles")

        if(levelData.toggles ~= nil) then

            -- Set the toggles counter to zero
            ToggleCnt = 0;
            

            for i=1,#levelData.toggles do

                local toggleImagePath = "characters/" .. levelData.toggles[i].name .. ".png"
                -- Add Mr Toggles
                toggles[i] = display.newImageRect( toggleImagePath, pngLib.getPngInfo(toggleImagePath).width, pngLib.getPngInfo(toggleImagePath).height )
                toggles[i]:setReferencePoint(display.CentreLeftReferencePoint)
                toggles[i].x, toggles[i].y = levelData.toggles[i].x, levelData.toggles[i].y
                toggles[i].xScale = levelData.toggles[i].xscale
                toggles[i].myName = "mrtoggles"
                
                -- Mr Toggles physics and collision
                physics.addBody( toggles[i], "static", { density=0, friction=0, bounce=0, isSensor=true } )     
                toggles[i].action = openDoor              
            end
        end
        
        if(levelData.clive ~= nil) then
            -- Clive Animation Data
            local imageSheet = grabImageSheet("characters/clive.png",
                                  gameData.animations["clive"].frames)
            
            -- Setup the s
            local sequenceData = {
                {name = "clive", frames=gameData.animations["clive"].sequence,
                 count=gameData.animations["clive"].count,
                 time=gameData.animations["clive"].time,
                 loopCount = gameData.animations["clive"].loopCount }
            }	            
            
            -- Add Evil Clive
            --clive = display.newImageRect( "characters/clive.png", pngLib.getPngInfo("characters/clive.png").width, pngLib.getPngInfo("characters/clive.png").height )
            clive = display.newSprite (imageSheet, sequenceData);
            clive:setReferencePoint(display.CentreLeftReferencePoint)
            clive.x, clive.y = levelData.clive.x, levelData.clive.y
            clive.xScale = levelData.clive.xscale
            clive.myName = "clive"
            clive:setSequence("clive");
            
            -- Mr Toggles physics and collision
            physics.addBody( clive, "dynamic", gamePhysics:get(clive.myName))    
            --clive.collision = onLocalCollision
            clive.action = killBob
            clive.active = false
            clive:addEventListener( "collision", clive )
            
            clive.isFixedRotation = true
        end
        
        print("Adding portal")
        -- The Door
        if(_G.currentLevelIndex ~= 10) then
            theDoor = display.newImageRect( "characters/dooropen.png", pngLib.getPngInfo("characters/dooropen.png").width, pngLib.getPngInfo("characters/dooropen.png").height );
            -- Load in closed door image
            theDoorClosed = display.newImage( "characters/doorclosed.png", pngLib.getPngInfo("characters/doorclosed.png").width, pngLib.getPngInfo("characters/doorclosed.png").height )            
        else
            theDoor = display.newImageRect( "characters/" .. _G.currentWorld .. "piece.png", pngLib.getPngInfo("characters/" .. _G.currentWorld .. "piece.png").width, pngLib.getPngInfo("characters/" .. _G.currentWorld .. "piece.png").height );
            theDoorClosed = display.newImage( "characters/padlock.png", pngLib.getPngInfo("characters/padlock.png").width, pngLib.getPngInfo("characters/padlock.png").height ) 
        end
	theDoor.x, theDoor.y = levelData.thedoor.x, levelData.thedoor.y
        theDoor.open = false
        
        -- Door physics and collision
        physics.addBody( theDoor, "static", { density=0, friction=0.6, bounce=0, isSensor=true } )      
        --theDoor.collision = onLocalCollision
        theDoor.myName = "thedoor"
        theDoor.action = completeLevel
        --theDoor:addEventListener( "collision", theDoor )       
	
    
	    -- Position the closed door image
        theDoorClosed.x, theDoorClosed.y = theDoor.x, theDoor.y
        
        print("Loading Animations");
        -- If there's animations, load them in
        if(levelData.animations ~= nil) then
            print(#levelData.animations);
            for i=1,#levelData.animations do                
                print("Adding " .. levelData.animations[i].name .. " Animation");
                local imageSheet = grabImageSheet("characters/" .. levelData.animations[i].name .. ".png",
                                                  worldData.animations[levelData.animations[i].name].frames)
            
                table.insert(imageSheets, imageSheet);
            end

        end
        
        print("Populating with Baddies")
        -- Load the baddies
        for i=1,#levelData.baddies do
            if(levelData.baddies[i].movementType == "dynamic") then
                baddies[i] = display.newImageRect("characters/" .. levelData.baddies[i].name .. ".png", pngLib.getPngInfo("characters/" .. levelData.baddies[i].name .. ".png").width, pngLib.getPngInfo("characters/" .. levelData.baddies[i].name .. ".png").height)
                baddies[i]:setReferencePoint(display.CentreLeftReferencePoint)
                baddies[i].x = levelData.baddies[i].x; baddies[i].y = levelData.baddies[i].y
                baddies[i].myName = levelData.baddies[i].name
                baddies[i].startX = baddies[i].x
                baddies[i].startY = baddies[i].y
                baddies[i].endX = levelData.baddies[i].endX
                baddies[i].endY = levelData.baddies[i].endY
                baddies[i].speedX = levelData.baddies[i].speedX
                baddies[i].speedY = levelData.baddies[i].speedY            
                baddies[i].flipped = levelData.baddies[i].flipped
                    if(baddies[i].flipped) then
                        baddies[i].xScale = -1;
                    end
                baddies[i].type = 1 -- numbers mean nothing, this is just to setup a baddy collision
                baddies[i].movementType = levelData.baddies[i].movementType
                baddies[i].animated = levelData.baddies[i].animated                
            elseif(levelData.baddies[i].movementType == "static") then
                if(levelData.baddies[i].animated) then
                    -- Setup the animation
                    local sequenceData = {
                        {name = levelData.baddies[i].name, frames=worldData.animations[levelData.baddies[i].name].sequence,
                         count=worldData.animations[levelData.baddies[i].name].count,
                         time=worldData.animations[levelData.baddies[i].name].time,
                         loopCount = worldData.animations[levelData.baddies[i].name].loopCount }
                    }

                    baddies[i] = display.newSprite (imageSheets[1], sequenceData);	
                    baddies[i]:setSequence(levelData.baddies[i].name); 
                    if(levelData.baddies[i].play) then
                        baddies[i]:play();                
                    end 	
                else
                    baddies[i] = display.newImageRect("characters/" .. levelData.baddies[i].name .. ".png", pngLib.getPngInfo("characters/" .. levelData.baddies[i].name .. ".png").width, pngLib.getPngInfo("characters/" .. levelData.baddies[i].name .. ".png").height)
                    baddies[i].type = 1 -- numbers mean nothing, this is just to setup a baddy collision
                end                    
                    baddies[i]:setReferencePoint(display.CentreLeftReferencePoint)
                    baddies[i].x = levelData.baddies[i].x; baddies[i].y = levelData.baddies[i].y
                    baddies[i].myName = levelData.baddies[i].name
                    baddies[i].flipped = levelData.baddies[i].flipped                    
                    baddies[i].movementType = levelData.baddies[i].movementType
                    baddies[i].animated = levelData.baddies[i].animated 
                    if(baddies[i].flipped) then
                        baddies[i].xScale = -1;
                    end
                
            end
        
            
            -- Add the physics
            physics.addBody( baddies[i], "static", worldPhysics:get(baddies[i].myName));
            --baddies[i].collision = onLocalCollision
            baddies[i].action = killBob
            --baddies[i]:addEventListener( "collision", baddies[i] )  
        end
        
        -- Create the HUD       
        print ("Adding Buttons")       
        -- Left / Right Button
        blastButton = display.newImageRect( "interface/blast.png", pngLib.getPngInfo("interface/blast.png").width, pngLib.getPngInfo("interface/blast.png").height )
        blastButton:setReferencePoint( display.BottomRightReferencePoint )
        blastButton.x = display.contentWidth
        blastButton.y = display.contentHeight
        blastButton.alpha = 1
        blastButton.touch = buttonHandler

        -- Thruster Button Event Listener
        blastButton:addEventListener("touch",blastButton)
        blastButton.pressed = false
        blastButton.alphaNotActive = 1
        blastButton.alphaActive = 0.6       
        
        -- Left Button
        buttonLeft = display.newImageRect( "interface/leftbutton.png", pngLib.getPngInfo("interface/leftbutton.png").width, pngLib.getPngInfo("interface/leftbutton.png").height )
        buttonLeft:setReferencePoint( display.BottomLeftReferencePoint )        
        buttonLeft.x = 0; buttonLeft.y = display.contentHeight
        buttonLeft.touch = buttonHandler
        buttonLeft.alpha = 1
        
        -- Left Button Event Listener
        buttonLeft:addEventListener("touch",buttonLeft)       
        buttonLeft.pressed = false  
        buttonLeft.alphaNotActive = 1
        buttonLeft.alphaActive = 0.6
        
        -- Right Button
        buttonRight = display.newImageRect( "interface/rightbutton.png", pngLib.getPngInfo("interface/rightbutton.png").width, pngLib.getPngInfo("interface/rightbutton.png").height )
        buttonRight:setReferencePoint( display.BottomLeftReferencePoint )        
        buttonRight.x = buttonLeft.width; buttonRight.y = display.contentHeight
        buttonRight.touch = buttonHandler
        buttonRight.alpha = 1
        
        -- Left Button Event Listener
        buttonRight:addEventListener("touch",buttonRight)       
        buttonRight.pressed = false  
        buttonRight.alphaNotActive = 1
        buttonRight.alphaActive = 0.6
        
--        print("Creating Health Bar")
        -- Health bar image
--        healthBar = display.newImageRect( "interface/healthBar.png", pngLib.getPngInfo("interface/healthBar.png").width, pngLib.getPngInfo("interface/healthBar.png").height )
--        healthBar:setReferencePoint( display.TopLeftReferencePoint )
--        healthBar.x = display.contentWidth/2 - healthBar.width/2
--        healthBar.y = display.contentHeight-30
        
        -- Health Bar overlay
--	healthBarRect = display.newRect(0,0,0,healthBar.height)
--        healthBarRect:setReferencePoint( display.TopRightReferencePoint )--
--	healthBarRect.x = healthBar.x + healthBar.width
 --       healthBarRect.y = healthBar.y 
 --       healthBarRect:setFillColor( 0, 0, 0)
                
        -- Setting up countdown timer
        print("Setting Timer")

        -- Load in timer background
        local timerBack = display.newImage( "interface/timer.png" )
        timerBack.x = 48; timerBack.y = 18
        
        -- Create timer text
        txtTimer = display.newText(timerText, 20, 7, "Laserian", 17)
        txtTimer:setTextColor(255,0,0)
        
        -- Tick once a second
        countdownTimer = timer.performWithDelay(1000, countDown, 0)        
        
        -- Add Evil Clive image
        pauseButton = display.newImageRect( "interface/pausebutton.png", pngLib.getPngInfo("interface/pausebutton.png").width, pngLib.getPngInfo("interface/pausebutton.png").height )
        pauseButton.x = timerBack.x + timerBack.width - 26; pauseButton.y = 18
        
        pauseButton.touch = buttonHandler
        pauseButton.alpha = 1
        
        -- Left Button Event Listener
        pauseButton:addEventListener("touch",pauseButton)       
        pauseButton.pressed = false  
        pauseButton.alphaNotActive = 1
        pauseButton.alphaActive = 0.6        
        
        -- Level Over display
        print("Adding \"Level Over\" Display")
        
        -- Make the group invisible
        levelOverGroup.alpha = 0.0
        
	-- Setup dark background
        local levelOverBack = display.newImageRect("interface/scoresBG.png",display.contentWidth,display.contentHeight)
	levelOverBack:setReferencePoint( display.TopLeftReferencePoint )
        --levelOverBack:setFillColor(black)
	levelOverBack.x, levelOverBack.y = 0, 0                  
        
	-- create a widget button (which will load planetsmenu.lua on release)
	nextBtn = widget.newButton{
		defaultFile="interface/next.png",
		overFile="interface/nextOver.png",
		width=pngLib.getPngInfo("interface/next.png").width,
		height=pngLib.getPngInfo("interface/next.png").height,	
		onRelease = onNextBtnRelease	-- event listener function
	}
	nextBtn:setReferencePoint( display.CenterReferencePoint )
	nextBtn.x = display.contentWidth- nextBtn.width
	nextBtn.y = display.contentHeight - nextBtn.height


    bobFlame=CBE.VentGroup{
        {
            preset="burn",
            title="flame",
            color={{255, 0, 0}}, -- Ice blue
            build=function()
            --    local size=math.random(90, 120)
                return display.newImageRect("images/c1.png", 20, 20)
            end,
            --onCreation=function()end, -- Original "burn" preset changes color onCreation
            perEmit=1,
            positionType="inRadius",
            posRadius=10,
            emitDelay=50,
            fadeInTime=500,
            lifeSpan=1000,
            lifeStart=500,
            endAlpha=0,
            parentGroup=gameGroup,
            physics={

                sizeX=-0.005,
                sizeY=-0.005,
                autoAngle=false,
                angles={270},
                relativeToSize=false,
                velocity=2,
                xDamping=1,
                gravityY=-0.01,
                gravityX=-0.01
            }
        }
    }

    

    --VentGroup:emitMaster();


    explodeEm=CBE.VentGroup{
        {
            preset="fireworks",
            title = "fire",
            build=function()
                return display.newImageRect("images/c1.png", 10, 10 )
            end,
            emitDelay=500,
            fadeInTime=100,
            lifeSpan=100,
            lifeStart=500,
            positionType="atPoint",
            perEmit=8,
            physics={

                relativeToSize=false,
                velocity=2,
                gravity=0.2
            },
            parentGroup=gameGroup
        },
        {
            preset="fireworks",
            title = "smoke",
            build=function()
                return display.newImageRect("images/c2.png", 20, 20 )
            end,
            emitDelay=500,
            fadeInTime=100,
            lifeSpan=100,
            lifeStart=500,
            positionType="atPoint",
            perEmit=5,
            physics={

                relativeToSize=false,
                velocity=2,
                gravity=0.2
            },
            parentGroup=gameGroup
        }


    }
    

 
        
                -- Adding foreground
        if(levelData.foreground ~= nil ) then                        
            if(#levelData.foreground > 0) then

                for i=1,#levelData.foreground do
                    waterfalls[i] = display.newImageRect( "characters/waterfall.png", pngLib.getPngInfo("characters/waterfall.png").width, pngLib.getPngInfo("characters/waterfall.png").height )
                    waterfalls[i].x, waterfalls[i].y = levelData.foreground[i].x, levelData.foreground[i].y
                    waterfalls[i].alpha = 0
                    waterfalls[i].myName = "waterfall"
                    physics.addBody( waterfalls[i], "static", gamePhysics:get(waterfalls[i].myName) )
                    

                    waterEm[i]=CBE.VentGroup{ {
                                preset="waterfall",
                                positionType="alongLine", 
                                point1={waterfalls[i].x, waterfalls[i].y - 25}, 
                                point2={waterfalls[i].x, waterfalls[i].y - 10},
                                build=function()
                                    return display.newImageRect("characters/waterfall.png", 60, 40 )
                                end,
                                parentGroup=foregroundGroup,
                                perEmit=1,
                                alpha=0.4,
                                fadeInTime=20,
                                emitDelay=1,
                                physics={
                                    xDamping=1,
                                    sizeX=0,
                                    sizeY=0.1, -- This waterfall grows Y so that it looks more like a "falling" effect
                                    maxY=0.2,
                                    gravityY=0.05,
                                    relativeToSize=false,
                                    velocity=0
                                }
                            }
                        }

                        waterEm[i]:startMaster()
                        foregroundGroup:insert( waterfalls[i] )

                end
                               
            end
        end
        
        foregroundGroup.alpha = 0.8
        
        
        -- Create the pause screen
        pauseScreen = display.newImageRect("interface/pauseBG.png", display.contentWidth, display.contentHeight)
        pauseScreen:setReferencePoint( display.TopLeftReferencePoint )
        pauseScreen.x = 0
        pauseScreen.y = 0
        
	-- create a widget button (which will loads level1.lua on release)
	muteButton = widget.newButton{
		defaultFile="interface/mainmenu/muteButton.png",
		overFile="interface/mainmenu/muteButtonOver.png",
		width=pngLib.getPngInfo("interface/mainmenu/muteButton.png").width,
		height=pngLib.getPngInfo("interface/mainmenu/muteButton.png").height,			
		--onRelease = onStoryBtnRelease	-- event listener function
	}
	muteButton:setReferencePoint( display.TopLeftReferencePoint )
	muteButton.x = 446
	muteButton.y = 287
        
	-- create a widget button (which will load planetsmenu.lua on release)
	resumeGameBtn = widget.newButton{
		defaultFile="interface/resumeGame.png",
		overFile="interface/resumeGameOver.png",
		width=pngLib.getPngInfo("interface/resumeGame.png").width,
		height=pngLib.getPngInfo("interface/resumeGame.png").height,	
		onRelease = resumeGame	-- event listener function
	}
	resumeGameBtn:setReferencePoint( display.TopLeftReferencePoint )
	resumeGameBtn.x = 50
	resumeGameBtn.y = 120
        
	-- create a widget button (which will load planetsmenu.lua on release)
	exitBtn = widget.newButton{
		defaultFile="interface/exitButton.png",
		overFile="interface/exitButtonOver.png",
		width=pngLib.getPngInfo("interface/exitButton.png").width,
		height=pngLib.getPngInfo("interface/exitButton.png").height,	
		onRelease = exitGame	-- event listener function
	}
	exitBtn:setReferencePoint( display.TopLeftReferencePoint )
	exitBtn.x = 50
	exitBtn.y = 180        

        if(_G.currentLevelIndex == 10) then
            local completeScreenPath = "story/" .. _G.currentWorld .. "complete.png";

            completeScreen = display.newImageRect(completeScreenPath, pngLib.getPngInfo(completeScreenPath).width, pngLib.getPngInfo(completeScreenPath).height);
            completeScreen:setReferencePoint( display.TopLeftReferencePoint ); 
            completeScreen.x = display.contentWidth;
            completeScreen.y = 0;                     
        end   
        
        
        print("Inserting background")
        -- Insert background image
        bkgGroup:insert(bg)
        
       -- foregroundGroup:insert( waterfall )
        
        print("Inserting game entities")        
        gameGroup:insert( platforms )
        gameGroup:insert( borderBottom )
        for i=1,#toggles do
            gameGroup:insert( toggles[i] )
        end        
        --gameGroup:insert( mrtoggles )
        gameGroup:insert( theDoor )
        gameGroup:insert( theDoorClosed )
        gameGroup:insert( bob )
        gameGroup:insert( bob.text )
        for i=1,#baddies do
            gameGroup:insert( baddies[i] )
        end
        
        -- If Evil Clive exists, add him to the game group
        if(clive ~= nil) then
            gameGroup:insert( clive )
        end
        
        -- This group appears in the foreground of game objects
        gameGroup:insert( foregroundGroup )
        
        print("Inserting \"Level Over\" box")
        levelOverGroup:insert( levelOverBack )
        --levelOverGroup:insert( levelOverMessage )
        --levelOverGroup:insert( restartButton )
        --levelOverGroup:insert( menuButton )
        levelOverGroup:insert( nextBtn )

        if(_G.currentLevelIndex == 10) then
            completeGroup:insert(completeScreen);
        end

        print("Inserting HUD")
        hudGroup:insert( blastButton )        
        hudGroup:insert( buttonRight )
        hudGroup:insert( buttonLeft )
        --hudGroup:insert( healthBar )
       -- hudGroup:insert( healthBarRect )        
        hudGroup:insert( timerBack )
        hudGroup:insert( txtTimer )
        hudGroup:insert( pauseButton )
        hudGroup.alpha = 0.8
        
        pauseGroup:insert( pauseScreen )
        pauseGroup:insert( muteButton )
        pauseGroup:insert( resumeGameBtn )
        pauseGroup:insert( exitBtn )
        pauseGroup.alpha = 0

        -- Insert into main group
        group:insert( bkgGroup )
        group:insert( gameGroup )
        group:insert( levelOverGroup)         
        group:insert( hudGroup )
        group:insert( pauseGroup )
        
        hudGroup:toFront()
        completeGroup:toFront();
        levelOverGroup:toFront()
        pauseGroup:toFront()

        -- set initial camera y position (if bob is above halfway, set camera to top, otherwise set to bottom)
        if bob.y < display.contentHeight / 2 then
            gameGroup.y = 0
        else
            gameGroup.y=(-platforms.height)+display.contentHeight+10
        end
        
        -- set initial camera x position (if bob is above halfway, set camera to top, otherwise set to bottom)
        if bob.x < display.contentWidth / 2 then
            gameGroup.x = 0
        else
            gameGroup.x=-platforms.width + (display.contentWidth)
        end        



        -- Temporary vars

        --theDoor.open = true
        --theDoor.y = 10
        --bob.y = 10
        
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	physics.start()
	physics.setGravity(-10)	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

    CBE.DeleteAll()
    VentGroup = nil
        
        baddies = nil
        
	physics.stop()
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	package.loaded[physics] = nil
	physics = nil
        
        collectgarbage('collect')
end


-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )



Runtime:addEventListener("enterFrame", mainLoop)

-----------------------------------------------------------------------------------------

return scene