-- This file is for use with Corona(R) SDK
--
-- This file is automatically generated with PhysicsEdtior (http://physicseditor.de). Do not edit
--
-- Usage example:
--			local scaleFactor = 1.0
--			local physicsData = (require "shapedefs").physicsData(scaleFactor)
--			local shape = display.newImage("objectname.png")
--			physics.addBody( shape, physicsData:get("objectname") )
--

-- copy needed functions to local scope
local unpack = unpack
local pairs = pairs
local ipairs = ipairs

local M = {}

function M.physicsData(scale)
	local physics = { data =
	{ 
		
		["level1"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -464, 205  ,  -455, 176  ,  -448, 207  ,  -456, 220  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -448, 207  ,  -455, 176  ,  -423, 206  ,  -440, 224  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -368, 204  ,  -414, 159  ,  -379, 159  ,  -359, 171  ,  -353, 197  ,  -359, 212  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -378, 203  ,  -368, 204  ,  -370, 212  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -405, 205  ,  -389, 201  ,  -398, 213  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -353, 197  ,  -339, 182  ,  -317, 193  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -353, 197  ,  -359, 171  ,  -339, 182  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -389, 201  ,  -378, 203  ,  -386, 223  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -418, 215  ,  -405, 205  ,  -410, 223  ,  -414, 229  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -423, 206  ,  -405, 205  ,  -418, 215  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -368, 204  ,  -378, 203  ,  -389, 201  ,  -455, 176  ,  -414, 159  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -455, 176  ,  -464, 205  ,  -477, 220  ,  -478, 216  ,  -474, 196  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -455, 176  ,  -389, 201  ,  -405, 205  ,  -423, 206  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -221, 169  ,  -213, 162  ,  -210, 166  ,  -215, 173  ,  -222, 176  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -112, 165  ,  -155, 159  ,  -140, 151  ,  -117, 152  ,  -103, 155  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -202, 156  ,  -195, 160  ,  -210, 166  ,  -213, 162  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -201, 148  ,  -190, 140  ,  -187, 149  ,  -195, 160  ,  -202, 156  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -155, 159  ,  -165, 160  ,  -177, 151  ,  -165, 149  ,  -140, 151  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -177, 151  ,  -187, 149  ,  -190, 140  ,  -172, 144  ,  -165, 149  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -201, 148  ,  -226, 148  ,  -228, 141  ,  -190, 140  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 1 },
                    shape = {   463, 169  ,  415, 177  ,  350, 176  ,  357, 158  ,  477, 154  ,  478, 165  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 1 },
                    shape = {   318, 158  ,  357, 158  ,  350, 176  ,  318, 173  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 1 },
                    shape = {   415, 177  ,  463, 169  ,  446, 182  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   252, 14  ,  273, 11  ,  263, 32  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   224, 19  ,  252, 14  ,  240, 50  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   168, 19  ,  195, -31  ,  190, 28  ,  180, 41  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   155, 1  ,  195, -31  ,  168, 19  ,  149, 38  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   190, 28  ,  195, -31  ,  258, -51  ,  224, 19  ,  201, 47  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   314, 21  ,  321, 4  ,  321, 26  ,  316, 36  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   273, 11  ,  301, -44  ,  289, 12  ,  279, 44  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   301, -44  ,  252, 14  ,  224, 19  ,  258, -51  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   305, 14  ,  301, -44  ,  321, 4  ,  314, 21  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   346, 6  ,  362, -6  ,  377, -4  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   346, 6  ,  321, 4  ,  301, -44  ,  362, -6  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   298, 25  ,  305, 14  ,  298, 40  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   289, 12  ,  301, -44  ,  305, 14  ,  298, 25  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   252, 14  ,  301, -44  ,  273, 11  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -86, -219  ,  -98, -278  ,  -75, -281  ,  -67, -216  ,  -77, -197  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -153, -248  ,  -136, -224  ,  -163, -223  ,  -189, -233  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -129, -265  ,  -98, -278  ,  -103, -219  ,  -119, -215  ,  -136, -224  ,  -153, -248  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -5, -206  ,  15, -245  ,  17, -214  ,  5, -192  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -136, -224  ,  -119, -215  ,  -135, -195  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -39, -215  ,  -5, -206  ,  -15, -183  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -67, -216  ,  -75, -281  ,  -44, -275  ,  -39, -215  ,  -54, -174  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   17, -214  ,  15, -245  ,  38, -208  ,  37, -189  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -39, -215  ,  -44, -275  ,  -4, -257  ,  15, -245  ,  -5, -206  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -103, -219  ,  -92, -206  ,  -93, -188  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -103, -219  ,  -98, -278  ,  -86, -219  ,  -92, -206  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -119, -215  ,  -113, -206  ,  -113, -192  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -119, -215  ,  -103, -219  ,  -113, -206  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -452, -106  ,  -440, -107  ,  -447, -96  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -410, -104  ,  -392, -115  ,  -395, -106  ,  -404, -91  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -426, -105  ,  -410, -104  ,  -414, -86  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -482, -112  ,  -481, -129  ,  -465, -136  ,  -468, -109  ,  -476, -93  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -438, -139  ,  -411, -130  ,  -392, -115  ,  -440, -107  ,  -452, -106  ,  -461, -107  ,  -468, -109  ,  -465, -136  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -440, -107  ,  -426, -105  ,  -435, -84  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -461, -107  ,  -452, -106  ,  -455, -90  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -468, -109  ,  -461, -107  ,  -465, -92  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -395, -106  ,  -392, -115  ,  -386, -89  }
                    }
                     ,
                    {
                    pe_fixture_id = "leafok", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -426, -105  ,  -440, -107  ,  -392, -115  ,  -410, -104  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   154, -172  ,  185, -165  ,  194, -160  ,  163, -161  ,  149, -166  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   194, -160  ,  268, -161  ,  268, -155  ,  209, -149  }
                    }
                     ,
                    {
                    pe_fixture_id = "branchhurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   194, -160  ,  185, -165  ,  268, -161  }
                    }
                    
                    
                    
                     ,
                    
                    
                    {
                    pe_fixture_id = "planthurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   233, -165  ,  230, -191  ,  238, -207  ,  248, -205  ,  248, -177  ,  246, -164  }
                    }
                     ,
                    {
                    pe_fixture_id = "planthurt", density = 2, friction = 2, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   274, -201  ,  248, -177  ,  248, -205  ,  270, -210  }
                    }
                    
                    
                    
		}
		
		, 
		["flyer"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "flyerflipped", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -22, -4.5  ,  -15, -8.5  ,  -1, -10.5  ,  6, -5.5  ,  -14, 9.5  ,  -20, 9.5  ,  -27, 2.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "flyerflipped", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -1, -10.5  ,  -15, -8.5  ,  -15, -16.5  ,  -9, -20.5  ,  -1, -17.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "flyerflipped", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -13, 17.5  ,  -14, 9.5  ,  -1, 8.5  ,  -2, 15.5  ,  -7, 19.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "flyerflipped", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   10, 2.5  ,  6, -5.5  ,  27, -4.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "flyerflipped", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -1, 8.5  ,  -14, 9.5  ,  6, -5.5  ,  10, 2.5  }
                    }
                    
                    
                    
		}
		
		, 
		["bob"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   13, -13  ,  -14.5, -37.5  ,  13.5, -20.5  ,  16.5, -15.5  ,  16.5, -11.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   16, -5.5  ,  11, -5.5  ,  14.5, -8.5  ,  19, -7  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -17.5, -19.5  ,  -14.5, -23.5  ,  -9, -6.5  ,  -17, -5.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   19.5, -29.5  ,  -14.5, -37.5  ,  -11, -45.5  ,  21.5, -31.5  ,  22.5, -30.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   13.5, -20.5  ,  15.5, -22.5  ,  16.5, -21.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   11, -5.5  ,  3, -2.5  ,  -2, -5.5  ,  -14.5, -23.5  ,  -14.5, -37.5  ,  13.5, -10.5  ,  14.5, -8.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   13.5, -20.5  ,  -14.5, -37.5  ,  19.5, -29.5  ,  20.5, -27.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -14.5, -23.5  ,  -2, -5.5  ,  -9, -6.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   21.5, -31.5  ,  -11, -45.5  ,  21, -44.5  ,  23.5, -39.5  ,  23.5, -33.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "bob", density = 0.4, friction = 1, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -14.5, -37.5  ,  13, -13  ,  13.5, -10.5  }
                    }
                    
                    
                    
		}
		
		, 
		["toggles"] = {
                    
                    
                    
                    
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   8, 6  ,  7, 5  ,  9, 5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -12.5, -26.5  ,  -11.5, -26.5  ,  -12, -24  ,  -14, -26  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -26, -5  ,  -20, -11  ,  -8.5, 2.5  ,  -21, 2  ,  -25.5, 0.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -3.5, 3.5  ,  8, 6  ,  -6, 9  ,  -8, 7  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -8, 11  ,  -6, 9  ,  8, 6  ,  7.5, 12.5  ,  5.5, 14.5  ,  -6.5, 16.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   7, 1  ,  -11.5, -26.5  ,  -0.5, -22.5  ,  6.5, -18.5  ,  14, -7  ,  14, -2  ,  10, 2  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -0.5, -22.5  ,  -11.5, -26.5  ,  -2.5, -26.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   30, 15  ,  29, 24  ,  25.5, 26.5  ,  15.5, 26.5  ,  11, 17  ,  16.5, 11.5  ,  27.5, 10.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   11, 17  ,  15.5, 26.5  ,  11.5, 22.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -19, -19  ,  -12.5, -22.5  ,  -4.5, 1.5  ,  -8.5, 2.5  ,  -20, -11  ,  -21, -15  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -25.5, 10.5  ,  -26, 8  ,  -21, 2  ,  -8.5, 2.5  ,  -21, 13  ,  -22.5, 13.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   11, 17  ,  7.5, 12.5  ,  8, 6  ,  16.5, 11.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -3, 2  ,  7, 5  ,  8, 6  ,  -3.5, 3.5  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   7, 5  ,  -3, 2  ,  -12, -24  ,  -11.5, -26.5  ,  7, 1  }
                    }
                     ,
                    {
                    pe_fixture_id = "", density = 2, friction = 0, bounce = 0, 
                    filter = { categoryBits = 1, maskBits = 65535, groupIndex = 0 },
                    shape = {   -12.5, -22.5  ,  -12, -24  ,  -3, 2  ,  -4.5, 1.5  }
                    }
                    
                    
                    
		}
		
	} }

        -- apply scale factor
        local s = scale or 1.0
        for bi,body in pairs(physics.data) do
                for fi,fixture in ipairs(body) do
                    if(fixture.shape) then
                        for ci,coordinate in ipairs(fixture.shape) do
                            fixture.shape[ci] = s * coordinate
                        end
                    else
                        fixture.radius = s * fixture.radius
                    end
                end
        end
	
	function physics:get(name)
		return unpack(self.data[name])
	end

	function physics:getFixtureId(name, index)
                return self.data[name][index].pe_fixture_id
	end
	
	return physics;
end

return M

