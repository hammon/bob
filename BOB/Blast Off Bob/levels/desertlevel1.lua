display.setStatusBar( display.HiddenStatusBar )

local physics = require( "physics" )
physics.start()
--physics.setDrawMode( "hybrid" )
--physics.setDrawMode( "debug" )

local dispObj_1 = display.newImageRect( "desertlevel1.jpg", 960, 480 )
dispObj_1:setReferencePoint( display.TopLeftReferencePoint )
dispObj_1.x = 25
dispObj_1.y = 25

local startingPoint = display.newImageRect( "image.png", 52, 13 )
startingPoint.x = 51
startingPoint.y = 136
physics.addBody( startingPoint, { density=1, friction=0.3, bounce=0.2 } )
startingPoint.isSensor = true

