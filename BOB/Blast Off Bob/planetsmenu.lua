-----------------------------------------------------------------------------------------
--
-- planetsmenu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "widget" library
local widget = require "widget"

--------------------------------------------

-- forward declarations and other locals
-- Load png library for extracting info from a png without opening it
local pngLib = require("pngLib")

local jungle = nil
local lava = nil
local ice = nil
local desert = nil
local space = nil
local weather = nil
local cave = nil
local evilclive = nil

local deleteAudio = function ()
	print("Deleting Audio")
	audio.stop()
	--audio.dispose( menuMusic )
	audio.dispose( selectButton )
	--menuMusic = nil
	--print(menuMusic)
	selectButton = nil
end

local loadLevel = function ()
	deleteAudio()
	storyboard.gotoScene("reset", "fade", 250 )
end

-- 'onRelease' event listener for playBtn
local function onPlayBtnRelease()
	
	-- go to level1.lua scene
	_G.currentLevel = "levelloader"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

-- 'onRelease' event listener for storyBtn
local function onStoryBtnRelease()
	
	-- Display the story
	_G.currentLevel = "storypage1"
	audio.play(selectButton, {onComplete=loadLevel})
	
	return true	-- indicates successful touch
end

local onButtonRelease = function( event )
	local btn = event.target

        _G.currentWorld = btn.myName
	
	print(_G.currentWorld)

	local GGData = require( "modules.GGData" )
		
	-- If it's the Jungle level and first time, this will unlock the level
	if(_G.currentWorld == "jungle") then
		
		local unlockOne = GGData:new( _G.currentWorld .. "Data" )
		
		local levelOne = unlockOne:get("level1")
		
		if(levelOne == nil) then
			unlockOne:set("level1", 0)
			unlockOne:save()
		end
	end


	
	_G.currentLevel = "levelsmenu"	
	audio.play(selectButton, {onComplete=loadLevel})
end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	print("loading menu")
	--menuMusic = audio.loadStream("sfx/menumusic.wav")
	selectButton = audio.loadStream("sfx/select.wav")
	
	--audio.play( menuMusic, {loops=-1} )
	
	-- display a background image
	local background = display.newImageRect( "interface/planetsmenu.jpg", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
	
	-- create a widget button (which will loads level1.lua on release)
	jungle = widget.newButton{
		defaultFile="interface/jungleLevel.png",
		overFile="interface/jungleLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/jungleLevel.png").width,
		height=pngLib.getPngInfo("interface/jungleLevel.png").height
	}
	jungle:setReferencePoint( display.TopLeftReferencePoint )
	jungle.x = 12
	jungle.y = 38
	jungle.myName = "jungle"
	
	-- create a widget button (which will loads level1.lua on release)
	lava = widget.newButton{
		defaultFile="interface/lavaLevel.png",
		overFile="interface/lavaLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/lavaLevel.png").width,
		height=pngLib.getPngInfo("interface/lavaLevel.png").height		
	}
	lava:setReferencePoint( display.TopLeftReferencePoint )
	lava.x = 135
	lava.y = 53
	lava.myName = "lava"
	
	-- create a widget button (which will loads level1.lua on release)
	ice = widget.newButton{
		defaultFile="interface/iceLevel.png",
		overFile="interface/iceLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/iceLevel.png").width,
		height=pngLib.getPngInfo("interface/iceLevel.png").height		
	}
	ice:setReferencePoint( display.TopLeftReferencePoint )
	ice.x = 249
	ice.y = 45
	ice.myName = "ice"
	
	-- create a widget button (which will loads level1.lua on release)
	desert = widget.newButton{
		defaultFile="interface/desertLevel.png",
		overFile="interface/desertLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/desertLevel.png").width,
		height=pngLib.getPngInfo("interface/desertLevel.png").height		
	}
	desert:setReferencePoint( display.TopLeftReferencePoint )
	desert.x = 365
	desert.y = 40
	desert.myName = "desert"
	
	-- create a widget button (which will loads level1.lua on release)
	space = widget.newButton{
		defaultFile="interface/spaceLevel.png",
		overFile="interface/spaceLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/spaceLevel.png").width,
		height=pngLib.getPngInfo("interface/spaceLevel.png").height		
	}
	space:setReferencePoint( display.TopLeftReferencePoint )
	space.x = 351
	space.y = 171
	space.myName = "space"
	
	-- create a widget button (which will loads level1.lua on release)
	weather = widget.newButton{
		defaultFile="interface/weatherLevel.png",
		overFile="interface/weatherLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/weatherLevel.png").width,
		height=pngLib.getPngInfo("interface/weatherLevel.png").height		
	}
	weather:setReferencePoint( display.TopLeftReferencePoint )
	weather.x = 225
	weather.y = 172
	weather.myName = "weather"
	
	-- create a widget button (which will loads level1.lua on release)
	cave = widget.newButton{
		defaultFile="interface/caveLevel.png",
		overFile="interface/caveLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/caveLevel.png").width,
		height=pngLib.getPngInfo("interface/caveLevel.png").height		
	}
	cave:setReferencePoint( display.TopLeftReferencePoint )
	cave.x = 116
	cave.y = 200
	cave.myName = "cave"
	
	-- create a widget button (which will loads level1.lua on release)
	evilclive = widget.newButton{
		defaultFile="interface/evilCliveLevel.png",
		overFile="interface/evilCliveLevelOver.png",
		onRelease = onButtonRelease,
		width=pngLib.getPngInfo("interface/evilCliveLevel.png").width,
		height=pngLib.getPngInfo("interface/evilCliveLevel.png").height		
	}
	evilclive:setReferencePoint( display.TopLeftReferencePoint )
	evilclive.x = 5
	evilclive.y = 126
	evilclive.myName = "clive"	
	
	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( jungle )
	group:insert( lava )
	group:insert( ice )
	group:insert( desert )	
	group:insert( space )
	group:insert( weather )
	group:insert( cave )
	group:insert( evilclive )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	

	
	if storyBtn then
		storyBtn:removeSelf()	-- widgets must be manually removed
		storyBtn = nil
	end
	
	if jungle then
		jungle:removeSelf()
		jungle = nil
	end
	
	if exitBtn then
		exitBtn:removeSelf()
		exitBtn = nil
	end
end

	

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )


-----------------------------------------------------------------------------------------

return scene