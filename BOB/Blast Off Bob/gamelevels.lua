
local unpack = unpack
local pairs = pairs
local ipairs = ipairs

local M = {}

function M.levelData()
	local levels =
        {
            data =
            {
                gamePhysicsFile = "levels.gamephysics",
                ["planet"] = {
                    ["jungle"] = "levels.jungle.planetphysics"
                },
		["jungle"] = {
                    {
                    name = "level1", 
                    physicsFile = "levels.jungle.level1"
                    }
                     ,
                    {
                    name = "level2", 
                    physicsFile = "levels.jungle.level2"
                    }
		}
            }
        }

	function levels:getPhysicsFile(name, index)
                return self.data[name][index].physicsFile
	end
        
        function levels:getGamePhysicsFile()
            return self.data.gamePhysicsFile
        end
        
        function levels:getWorldPhysicsFile(planet)
            return self.data["planet"][planet]
        end
	
	return levels;
end

return M

