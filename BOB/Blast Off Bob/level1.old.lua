-----------------------------------------------------------------------------------------
--
-- level1.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-- include Corona's "physics" library
local physics = require "physics"
--physics.setDrawMode("hybrid")
physics.start(); physics.pause()

-- Require the widget library
local widget = require( "widget" )

--------------------------------------------

-- forward declarations and other locals
local screenW, screenH, halfW = display.contentWidth, display.contentHeight, display.contentWidth*0.5
local bob = nil
local gameGroup = nil
local hudGroup = nil
local timesUpGroup = nil
local levelComplete = false

-- hit points
local WALLHITPOINTS = 4
local PLANTHITPOINTS = 8
local PLATFORMHITPOINTS = 3

--
local healthBar = nil

local mrtoggles, theDoor, theDoorClosed

-- Baddies --
local baddies = {}

local buttonRight, buttonLeft, buttonSmall

local xVelocity = 0

-- Countdown Timer Variables
local timerCnt = 60
local timerText = "1:00"
local timeOver = false
local textTimer = nil
local timesUp

local menuButton, restartButton

-- Load Audio
local thrustSound = audio.loadStream("sfx/thrust.mp3")

-- activate multitouch so multiple touches can press different buttons simultaneously
system.activate( "multitouch" )

------------------------------------------------------
-- Runtime enterFrame Event handler
------------------------------------------------------

-- Move Baddies Function
local moveBaddies = function ()
    -- Loop through all the baddies in the table and move them left and right
    for i=1,#baddies do
        if baddies[i].x > baddies[i].startX + baddies[i].moveLength or baddies[i].x < baddies[i].startX then
            baddies[i].moveSpeed = baddies[i].moveSpeed * -1
            baddies[i].xScale = baddies[i].xScale * -1
        end

        baddies[i].x = baddies[i].x + baddies[i].moveSpeed
    end	
		
end

-- Countdown Timer Function
local countDown = function()

    if not timeOver then
            timerCnt = timerCnt - 1
            if bob.timer > 0 then
                bob.timer = bob.timer - 1
            else
                bob.text.text = ""
                bob.text.alpha = 0
            end
    end
    
    if timerCnt > 9 then
            timerText = "0:" .. timerCnt
    elseif timerCnt < 0 then
            timeOver = true
    else
            timerText = "0:0" .. timerCnt
    end       
end

local restartPressed = 0

-- Main Game Loop
local function mainLoop()
    -- If the time has hit zero, then stop performing any actions and display
    -- Time's Up Message
    if timeOver or bob.dead or levelComplete then
           timesUpGroup.alpha = 0.8
           
           if bob.dead then
               bob.alpha = 0
               
               timesUp.text = "You Died!"
           end
           
           if levelComplete then
               bob.alpha = 0
               timesUp.text = "Completed!"
           end
                          
           if menuButton.pressed then
               -- Restart the level
               restartPressed = restartPressed + 1
               if restartPressed == 1 then               
                    print("*** Menu Pressed ***")
                    _G.currentLevel = nil
                    storyboard.gotoScene( "reset", "fade", 250 )
                end
           elseif restartButton.pressed then
               -- Restart the level
               restartPressed = restartPressed + 1
               if restartPressed == 1 then               
                    print("*** Resetting Level ***")
                    storyboard.gotoScene( "reset", "fade", 250 )
                end
           end
           
           return true
    end	
    
    -- Update countdown timer text
    txtTimer.text = timerText
    
    -- Move the baddies
    moveBaddies()
   
    -- Stop bob from falling off the end of the screen
    if bob.x<bob.width/2+1 then
        bob.x = bob.width/2+1
    end
   
    if bob.x>display.contentWidth-(bob.width/2) then
        bob.x = display.contentWidth-(bob.width/2)
    end
        
    local xForce = 0
    local yForce = 0

    -- Increase upward force for thrust
    if buttonSmall.pressed then
        audio.play( thrustSound, {channel=1, loops=1} )

    	yForce = -5      
    else
        audio.stop({ channel=1 } )
    end

    -- Increase force to the right if the right button is pressed
    if buttonRight.pressed then
        bob.xScale = 1 
	xForce = 1
    end
    
    -- Increase force to the left if the left button is pressed        
    if buttonLeft.pressed then
        bob.xScale = -1 
        xForce = -1
    end    
    
    if bob.timer <= 0 then    
        -- Apply resulting force to bob based on above calculations (move him)
        bob:applyForce(xForce,yForce, bob.x, bob.y)
            
            
        if bob.y > 300 then
            bob.dead = true
        end
        
    elseif bob.timer > 0 then
        bob.text.alpha = 1
        bob.text.x = bob.x + 2
        bob.text.y = bob.y - (bob.height * 0.5) - 8
        bob.text.text = bob.timer        
    end

end

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

local buttonHandler = function( self, event )
    if event.phase == "began" then      
   	
        print("began phase")        
        
        self.pressed = true
        
        -- Subsequent touch events will target button even if they are outside the stageBounds of button
        display.getCurrentStage():setFocus( self, event.id )
        self.isFocus = true 
        event.target.alpha = event.target.alphaActive
		               
    elseif self.isFocus then
        local bounds = self.stageBounds
        local x,y = event.x,event.y
        local isWithinBounds = 
            bounds.xMin <= x and bounds.xMax >= x and bounds.yMin <= y and bounds.yMax >= y
        
        if isWithinBounds and event.phase == "moved" then
            self.pressed = true
        elseif not isWithinBounds or event.phase == "ended" or event.phase == "cancelled" then
            self.isFocus = false
            display.getCurrentStage():setFocus( nil )
            self.pressed = false
            event.target.alpha = event.target.alphaNotActive
        end       
    end	
	
    return true
end

local reduceHealth = function( amount )
    bob.health = bob.health - amount
    healthBar.width = healthBar.width - amount

    healthBar:setReferencePoint( display.TopLeftReferencePoint )
    healthBar.x = display.contentWidth * 0.35

    if healthBar.width <= 75 and healthBar.width > 40 then
        healthBar:setFillColor( 255, 150, 0 )
    elseif healthBar.width == 40 then
        healthBar:setFillColor( 255, 0, 0 )
    end       
    
    if bob.health <= 0 then
        bob.health = 0
        healthBar.width = 0
        healthBar.alpha = 0
        bob.dead = true
        -- call bobDead function
    end
    
    print(bob.health)
end

local killBob = function ()
    bob.dead = true
end

local openDoor = function ()
    theDoor.open = true
    transition.to(mrtoggles, {alpha=0, time=500})
    transition.to(theDoorClosed, {alpha=0, time=1000})
end

local completeLevel = function ()
    if theDoor.open then
        levelComplete = true
    end
end

local stickyPlatform = function ()
    bob.timer = 10
    print("Sticky")
end

local onLocalCollision = function( self, event )
        if ( event.phase == "began" ) then
 
                print( "Collision began")
                self.action( self.hitAmount )
 
        elseif ( event.phase == "ended" ) then
 
                print("Collision ended" )
 
        end
        
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
        
        levelComplete = false
        
        print(display.contentHeight)

	gameGroup = display.newGroup()       
        hudGroup = display.newGroup()
        timesUpGroup = display.newGroup()

	-- create the level backdrop
	local background = display.newImageRect( "levels/desertlevel1.jpg", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0           

	
	-- make bob (off-screen), and position him
	bob = display.newImageRect( "bob.png", 19, 23 )
	bob.x, bob.y = 20, 30
        
        -- Bob stats
        bob.health = 150
        bob.dead = false
        bob.timer = 0
	
        -- For countdown timer above bob's head
        bob.text = display.newText("", bob.x + (bob.width * 0.5), bob.y - 2, "Laserian", 11)
        bob.text:setTextColor(255,0,0)
        bob.text.alpha = 0
        
	-- add physics to bob
	physics.addBody( bob, { density=0.7, friction=0.6, bounce=0.1 } )
	bob.isFixedRotation = true
        
        -- Mr toggles
	mrtoggles = display.newImageRect( "levels/mrtoggles.png", 14, 19 )
	mrtoggles.x, mrtoggles.y = 182, 254
        
        physics.addBody( mrtoggles, "static", { density=0, friction=0, bounce=0, isSensor=true } )
        
        mrtoggles.collision = onLocalCollision
        mrtoggles.action = openDoor
        mrtoggles:addEventListener( "collision", mrtoggles ) 
        
	theDoor = display.newImageRect( "levels/door.png", 25, 40 )
	theDoor.x, theDoor.y = 443, 27
        theDoor.open = false
        
        physics.addBody( theDoor, "static", { density=0, friction=0.6, bounce=0, isSensor=true } )
        
        theDoor.collision = onLocalCollision
        theDoor.action = completeLevel
        theDoor:addEventListener( "collision", theDoor ) 
        
	
        theDoorClosed = display.newImageRect( "levels/doorclosed.png", 25, 40 )
	theDoorClosed.x, theDoorClosed.y = 443, 27
        
        --transition.to(theDoorClosed, {alpha=0, time=800})
        
	-- create a bottom boundary object and add physics (with custom shape)
	local bottom = display.newRect(0,0,display.contentWidth,0)
	bottom:setReferencePoint( display.BottomLeftReferencePoint )
	bottom.x, bottom.y = 0, display.contentHeight
        
        -- Bottom Boundary Physics
	physics.addBody( bottom, "static", { friction=0.3, shape=bottomShape } )
        
        -- create a top boundary object and add physics (with custom shape)
	local top = display.newRect(0,0,display.contentWidth,0)
	top:setReferencePoint( display.TopLeftReferencePoint )
        top:setFillColor(0)
	top.x, top.y = 0, -1
        
        -- Bottom Boundary Physics
	physics.addBody( top, "static", { friction=0.3, shape=topShape } )

	-- *** CREATE LEVEL ***
        local startingPoint = display.newImageRect( "", 24, 9 )
        startingPoint:setReferencePoint( display.TopLeftReferencePoint )
        startingPoint.x = 0
        startingPoint.y = 70
        startingPoint.alpha = 0
        physics.addBody( startingPoint, "static", { density=1, friction=0.3, bounce=0.2 } )
                      
        local platform2 = display.newImageRect( "", 24, 7 )
        platform2:setReferencePoint( display.TopLeftReferencePoint )
        platform2.x = 0
        platform2.y = 132
        platform2.alpha = 0
        physics.addBody( platform2, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        platform2.collision = onLocalCollision
        platform2.action = reduceHealth
        platform2.hitAmount = PLATFORMHITPOINTS
        platform2:addEventListener( "collision", platform2 )     
        
        local wall1 = display.newImageRect( "", 10, 227 )
        wall1:setReferencePoint( display.TopLeftReferencePoint )
        wall1.x = 81
        wall1.y = 0
        wall1.alpha = 0
        physics.addBody( wall1, "static", { density=1, friction=0.3, bounce=0.6 } )
        
        wall1.collision = onLocalCollision
        wall1.action = reduceHealth
        wall1.hitAmount = WALLHITPOINTS
        wall1:addEventListener( "collision", wall1 )
        
        
        local quicksand1 = display.newImageRect( "", 30, 9 )
        quicksand1:setReferencePoint( display.TopLeftReferencePoint )
        quicksand1.x = 55
        quicksand1.y = 194
        quicksand1.alpha = 0
        physics.addBody( quicksand1, "static", { density=1, friction=0.3, bounce=0 } )
        
        quicksand1.collision = onLocalCollision
        quicksand1.action = stickyPlatform
        quicksand1:addEventListener( "collision", quicksand1 )          
               
        local quicksand2 = display.newImageRect( "", 30, 8 )
        quicksand2:setReferencePoint( display.TopLeftReferencePoint )
        quicksand2.x = 123
        quicksand2.y = 66
        quicksand2.alpha = 0
        physics.addBody( quicksand2, "static", { density=1, friction=0.3, bounce=0 } )
        
        quicksand2.collision = onLocalCollision
        quicksand2.action = stickyPlatform
        quicksand2:addEventListener( "collision", quicksand2 )         
        
        local lowerPlatform = display.newImageRect( "", 30, 8 )
        lowerPlatform:setReferencePoint( display.TopLeftReferencePoint )
        lowerPlatform.x = 122
        lowerPlatform.y = 71
        lowerPlatform.alpha = 0
        physics.addBody( lowerPlatform, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        lowerPlatform.collision = onLocalCollision
        lowerPlatform.action = reduceHealth
        lowerPlatform.hitAmount = PLATFORMHITPOINTS
        lowerPlatform:addEventListener( "collision", lowerPlatform )  
                
        local lowerPlatform2 = display.newImageRect( "", 30, 9 )
        lowerPlatform2:setReferencePoint( display.TopLeftReferencePoint )
        lowerPlatform2.x = 51
        lowerPlatform2.y = 199
        lowerPlatform2.alpha = 0
        physics.addBody( lowerPlatform2, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        lowerPlatform2.collision = onLocalCollision
        lowerPlatform2.action = reduceHealth
        lowerPlatform2.hitAmount = PLATFORMHITPOINTS
        lowerPlatform2:addEventListener( "collision", lowerPlatform2 )         
                
        local lowerPlatform3 = display.newImageRect( "", 30, 9 )
        lowerPlatform3:setReferencePoint( display.TopLeftReferencePoint )
        lowerPlatform3.x = 219
        lowerPlatform3.y = 267
        lowerPlatform3.alpha = 0
        physics.addBody( lowerPlatform3, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        lowerPlatform3.collision = onLocalCollision
        lowerPlatform3.action = reduceHealth
        lowerPlatform3.hitAmount = PLATFORMHITPOINTS
        lowerPlatform3:addEventListener( "collision", lowerPlatform3 )         
                
        local wall2 = display.newImageRect( "", 8, 265 )
        wall2:setReferencePoint( display.TopLeftReferencePoint )
        wall2.x = 150
        wall2.y = 55
        wall2.alpha = 0
        physics.addBody( wall2, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        wall2.collision = onLocalCollision
        wall2.action = reduceHealth
        wall2.hitAmount = WALLHITPOINTS
        wall2:addEventListener( "collision", wall2 )                
        
        local platform3 = display.newImageRect( "", 87, 7 )
        platform3:setReferencePoint( display.TopLeftReferencePoint )
        platform3.x = 160
        platform3.y = 268
        platform3.alpha = 0
        physics.addBody( platform3, "static", { density=1, friction=0.3, bounce=0.2 } )
              
        local wall3 = display.newImageRect( "", 9, 174 )
        wall3:setReferencePoint( display.TopLeftReferencePoint )
        wall3.x = 264
        wall3.y = 0
        wall3.alpha = 0
        physics.addBody( wall3, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        wall3.collision = onLocalCollision
        wall3.action = reduceHealth
        wall3.hitAmount = WALLHITPOINTS
        wall3:addEventListener( "collision", wall1 )               
        
        local platform4 = display.newImageRect( "", 78, 6 )
        platform4:setReferencePoint( display.TopLeftReferencePoint )
        platform4.x = 400
        platform4.y = 49
        platform4.alpha = 0
        physics.addBody( platform4, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        platform4.collision = onLocalCollision
        platform4.action = reduceHealth
        platform4.hitAmount = PLATFORMHITPOINTS
        platform4:addEventListener( "collision", platform4 )                  
        
        local platform5 = display.newImageRect( "", 45, 6 )
        platform5:setReferencePoint( display.TopLeftReferencePoint )
        platform5.x = 435
        platform5.y = 193
        platform5.alpha = 0
        physics.addBody( platform5, "static", { density=1, friction=0.3, bounce=0.2 } )
        
        local cactus2 = display.newImageRect( "", 22, 13 )
        cactus2:setReferencePoint( display.TopLeftReferencePoint )
        cactus2.x = 128
        cactus2.y = 196
        cactus2.alpha = 0
        physics.addBody( cactus2, "static", { density=1, friction=0.3, bounce=0.7, radius=11 } )
        
        cactus2.collision = onLocalCollision
        cactus2.action = reduceHealth
        cactus2.hitAmount = PLANTHITPOINTS
        cactus2:addEventListener( "collision", cactus2 )        
        
        local cactus4 = display.newImageRect( "", 18, 23 )
        cactus4:setReferencePoint( display.TopLeftReferencePoint )
        cactus4.x = 215
        cactus4.y = 250
        cactus4.alpha = 0
        physics.addBody( cactus4, "static", { density=1, friction=0.3, bounce=0.7, radius=0 } )
        
        cactus4.collision = onLocalCollision
        cactus4.action = reduceHealth
        cactus4.hitAmount = PLANTHITPOINTS
        cactus4:addEventListener( "collision", cactus4 )        
        
        local cactus1 = display.newImageRect( "", 18, 23 )
        cactus1:setReferencePoint( display.TopLeftReferencePoint )
        cactus1.x = 10
        cactus1.y = 113
        cactus1.alpha = 0
        physics.addBody( cactus1, "static", { density=1, friction=0.3, bounce=0.7, radius=0 } )

        cactus1.collision = onLocalCollision
        cactus1.action = reduceHealth
        cactus1.hitAmount = PLANTHITPOINTS
        cactus1:addEventListener( "collision", cactus1 )        
        
        local cactus3 = display.newImageRect( "", 22, 10 )
        cactus3:setReferencePoint( display.TopLeftReferencePoint )
        cactus3.x = 90
        cactus3.y = 133
        cactus3.alpha = 0
        physics.addBody( cactus3, "static", { density=1, friction=0.3, bounce=0.7, radius=11 } )
        
        cactus3.collision = onLocalCollision
        cactus3.action = reduceHealth
        cactus3.hitAmount = PLANTHITPOINTS
        cactus3:addEventListener( "collision", cactus3 )            
        
        local bricks = display.newImageRect( "", 19, 38 )
        bricks:setReferencePoint( display.TopLeftReferencePoint )
        bricks.x = 444
        bricks.y = 156
        bricks.alpha = 0
        physics.addBody( bricks, "static", { density=1, friction=0.3, bounce=0.2 } )
	
	-- *** END CREATE LEVEL ***
		
	-- *** SETUP THE BADDIES *** ---
        print("*** Creating Baddies ***")
        baddies[1] = display.newImageRect( "levels/greenbaddie.png", 39, 25 )
        baddies[1].x, baddies[1].y = 146, 20
        baddies[1].startX = baddies[1].x
        baddies[1].moveLength = baddies[1].width * 2 + 15
        baddies[1].moveSpeed = 2
    
        baddies[2] = display.newImageRect( "levels/greenbaddie.png", 39, 25 )
        baddies[2].x, baddies[2].y = 181, 102
        baddies[2].startX = baddies[2].x
        baddies[2].moveLength = baddies[2].width + 19
        baddies[2].moveSpeed = 1    
        
        baddies[3] = display.newImageRect( "levels/greenbaddie.png", 39, 25 )
        baddies[3].x, baddies[3].y = 183, 202
        baddies[3].startX = baddies[3].x
        baddies[3].moveLength = baddies[3].width * 6 - 3
        baddies[3].moveSpeed = 3	 
        
        baddies[4] = display.newImageRect( "levels/greenbaddie.png", 39, 25 )
        baddies[4].x, baddies[4].y = 297, 43
        baddies[4].startX = baddies[4].x
        baddies[4].moveLength = baddies[4].width * 2
        baddies[4].moveSpeed = 1
        
        for i=1,#baddies do
            physics.addBody( baddies[i], "static", { density=0.7, friction=0.6, bounce=0.1, radius=10 } )
            baddies[i].collision = onLocalCollision
            baddies[i].action = killBob
            baddies[i]:addEventListener( "collision", baddies[i] )               
            
        end 
        
        
        --- *** SETUP BUTTONS ***
        
        -- Thruster Button
        buttonSmall = display.newImage( "buttonArrow.png" )
        buttonSmall.x = 445; buttonSmall.y = 295
        buttonSmall.touch = buttonHandler
        -- Thruster Button Event Listener
        buttonSmall:addEventListener("touch",buttonSmall)
        buttonSmall.pressed = false
        buttonSmall.alphaNotActive = 0.8
        buttonSmall.alphaActive = 0.5

        -- Right Button
        buttonRight = display.newImage( "buttonRight.png" )
        buttonRight.x = 77; buttonRight.y = 293     
        -- Right Button Event Listener
        buttonRight.touch = buttonHandler
        buttonRight:addEventListener("touch",buttonRight)
        buttonRight.pressed = false
        buttonRight.alphaNotActive = 0.8
        buttonRight.alphaActive = 0.5
        
        -- Left Button
        buttonLeft = display.newImage( "buttonLeft.png" )
        buttonLeft.x = 27; buttonLeft.y = 293
        buttonLeft.touch = buttonHandler   
        -- Left Button Event Listener
        buttonLeft:addEventListener("touch",buttonLeft)       
        buttonLeft.pressed = false  
        buttonLeft.alphaNotActive = 0.8
        buttonLeft.alphaActive = 0.5
        
        -- *** SETUP COUNTDOWN TIMER ***
        local timerBack = display.newImage( "timer.png" )
        timerBack.x = 48; timerBack.y = 18
        timerBack.alpha = 0.6
        
        txtTimer = display.newText(timerText, 20, 7, "Laserian", 17)
        txtTimer:setTextColor(255,0,0)
        txtTimer.alpha = 0.8
        
        timer.performWithDelay(1000, countDown, 0)
        
        -- *** SETUP TIME'S UP DISPLAY
        timesUpGroup.alpha = 0.0
        
        -- create a top boundary object and add physics (with custom shape)
	local timesUpBack = display.newRect(0,0,display.contentWidth,display.contentHeight)
	timesUpBack:setReferencePoint( display.TopLeftReferencePoint )
        timesUpBack:setFillColor(black)
	timesUpBack.x, top.y = 0, 0       
        
        timesUp = display.newText("Time's Up!", 0, 0, "Laserian", 30)
        timesUp.x = display.contentWidth * 0.5; timesUp.y = display.contentHeight * 0.5 - 20
        timesUp:setTextColor(255,255,255)
        
        -- Health Bar
	healthBar = display.newRect(0,0,150,19)
        healthBar:setReferencePoint( display.TopLeftReferencePoint )
	healthBar.x = display.contentWidth * 0.35
        healthBar.y = display.contentHeight - 35
        healthBar:setFillColor( 0, 255, 0)
        healthBar.alpha = 0.8


        restartButton = display.newText("Replay", 0, 0, "Laserian", 18)
        restartButton.x = timesUp.x - (timesUp.width / 5); restartButton.y = timesUp.y + 28
        restartButton:setTextColor( 255, 255, 0 )      
        
        restartButton.touch = buttonHandler   
        restartButton:addEventListener("touch",restartButton)       
        restartButton.pressed = false
        restartButton.alphaNotActive = 1.0
        restartButton.alphaActive = 0.5
        
        menuButton = display.newText("Menu", 0, 0, "Laserian", 18)
        menuButton.x = restartButton.x + restartButton.width - 15; menuButton.y = restartButton.y
        menuButton:setTextColor( 255, 255, 0 )      
        
        menuButton.touch = buttonHandler   
        menuButton:addEventListener("touch", menuButton)       
        menuButton.pressed = false
        menuButton.alphaNotActive = 1.0
        menuButton.alphaActive = 0.5          
           
	-- all display objects must be inserted into group
	gameGroup:insert( bottom)
        gameGroup:insert( top )
        gameGroup:insert( mrtoggles )
        gameGroup:insert( theDoor )
        gameGroup:insert( theDoorClosed )        
	gameGroup:insert( bob )
        gameGroup:insert( bob.text )

        for i=1,#baddies do
            gameGroup:insert( baddies[i] )
        end
        hudGroup:insert( buttonSmall )
        hudGroup:insert( buttonRight )
        hudGroup:insert( buttonLeft )
        hudGroup:insert( timerBack )
        hudGroup:insert( txtTimer )
        hudGroup:insert( healthBar )
        timesUpGroup:insert( timesUpBack )
        timesUpGroup:insert( timesUp )
        timesUpGroup:insert( restartButton )
        timesUpGroup:insert( menuButton )
        
	group:insert( background )
	group:insert(gameGroup)
        group:insert(hudGroup)
        group:insert(timesUpGroup)
        --group:insert(testGroup)
	
	background:toBack()
        hudGroup:toFront()
        timesUpGroup:toFront()
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	physics.start()
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
        baddies = nil
        
	physics.stop()
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	--package.loaded[physics] = nil
	--physics = nil
end


-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )



Runtime:addEventListener("enterFrame", mainLoop)

-----------------------------------------------------------------------------------------

return scene