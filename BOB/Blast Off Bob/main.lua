-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

_G.currentLevel = "levelloader"
_G.currentLevelIndex = 1
_G.currentWorld = "lava"

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

--local sysFonts = native.getFontNames()
--for k,v in pairs (sysFonts) do print(v) end
    

-- load menu screen
--storyboard.gotoScene( "titlepage" )
--storyboard.gotoScene( "storypage1" )
--storyboard.gotoScene( "mainmenu" )
storyboard.gotoScene("levelloader")