-- This file was generated using SpriteHelper 2
-- For more informations please visit http://www.gamedevhelper.com/spritehelper2

module(...)

function getSpriteSheetData()

	local options = {
		-- array of tables representing each frame (required)
		frames = {

				--FRAME "ftframe1.png"
				{
					x = 1,
					y = 414,
					width = 64,
					height = 73,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 124,
					sourceY = 29
				},


				--FRAME "ftframe2.png"
				{
					x = 80,
					y = 411,
					width = 72,
					height = 75,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 123,
					sourceY = 27
				},


				--FRAME "ftframe3.png"
				{
					x = 1,
					y = 335,
					width = 77,
					height = 77,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 123,
					sourceY = 25
				},


				--FRAME "ftframe4.png"
				{
					x = 160,
					y = 170,
					width = 87,
					height = 78,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 124,
					sourceY = 24
				},


				--FRAME "ftframe5.png"
				{
					x = 160,
					y = 91,
					width = 91,
					height = 78,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 123,
					sourceY = 24
				},


				--FRAME "ftframe6.png"
				{
					x = 118,
					y = 331,
					width = 97,
					height = 78,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 113,
					sourceY = 24
				},


				--FRAME "ftframe7.png"
				{
					x = 1,
					y = 254,
					width = 115,
					height = 80,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 95,
					sourceY = 22
				},


				--FRAME "ftframe8.png"
				{
					x = 135,
					y = 250,
					width = 115,
					height = 80,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 96,
					sourceY = 22
				},


				--FRAME "ftframe9.png"
				{
					x = 1,
					y = 172,
					width = 132,
					height = 80,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 79,
					sourceY = 22
				},


				--FRAME "ftframe10.png"
				{
					x = 1,
					y = 91,
					width = 157,
					height = 80,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 54,
					sourceY = 22
				},


				--FRAME "ftframe11.png"
				{
					x = 1,
					y = 1,
					width = 204,
					height = 88,
					sourceWidth = 224,
					sourceHeight = 120,
					sourceX = 7,
					sourceY = 14
				},

		},
		sheetContentWidth = 251,
		sheetContentHeight = 487
	}
	return options
end

function getFrameNamesMap()
	local frameIndexes =
	{
		["ftframe1.png"] = 1,
		["ftframe2.png"] = 2,
		["ftframe3.png"] = 3,
		["ftframe4.png"] = 4,
		["ftframe5.png"] = 5,
		["ftframe6.png"] = 6,
		["ftframe7.png"] = 7,
		["ftframe8.png"] = 8,
		["ftframe9.png"] = 9,
		["ftframe10.png"] = 10,
		["ftframe11.png"] = 11,
	}
	return frameIndexes;
end

function getFramesCount()
	return 11;
end

function getFrameForName(name)
	return getFrameNamesMap()[name];
end
