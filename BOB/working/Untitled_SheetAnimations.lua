-- This file was generated using SpriteHelper 2
-- For more informations please visit http://www.gamedevhelper.com/spritehelper2

module(...)

function getSequenceData()

	local sequenceData = {
		{
			name = "Untitled",
			frames={1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, },
			time=238.095336,
			loopCount = 1
		},
	}
return sequenceData
end

function getSequenceWithName(name)

	local seq = getSequenceData();
	for i =1, #seq do
		if seq[i].name == name then
			return { seq[i] };
		end
	end
return nil;
end
